package com.rest;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import model.Usuario;
import services.UsuariosBeanRemote;

@Path("/usuarios")
@Stateless
public class Usuarios {
	
	@EJB(name="UsuariosBeanRemote")
	private UsuariosBeanRemote usuariosBean;
	
	@GET
	@Path("usuario/")
	public String usuario(){
		
		for (Usuario u : usuariosBean.SelectUsers()) {
			System.out.println( "Usuario [idUsuario=" + u.getIdUsuario() + ", apellido=" + u.getApellido() + ", cedula="
					+ u.getCedula() + ", contrasenia=" + u.getContrasenia() + ", email=" + u.getEmail() + ", nombUsuario=" + u.getNombUsuario() + ", nombre=" + u.getNombre() );	
		}

		return "bien";
		
	}

}
