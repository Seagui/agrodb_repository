package daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.MetoMuestreo;

@Stateless
public class DAOMetoMuestreo implements DAOMetoMuestreoLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAOMetoMuestreo() { }

	@Override
	public void CreateMethod(MetoMuestreo method) throws ServiciosException {
		try {
			if(!AlredyExists(method)) {	//Antes de Crear el Metodo de Muestreo se verifica que no exista con otra ID
				em.persist(method);
				em.flush();
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear el Metodo de Muestreo: " + method.getMetodo()); } 
	}

	@Override
	public List<MetoMuestreo> SelectMethods() {
		TypedQuery<MetoMuestreo> query = em.createNamedQuery("MetoMuestreo.findAll", MetoMuestreo.class);
		
		return query.getResultList();
	}

	@Override
	public Long GetIDMethod(String name) {
		TypedQuery<MetoMuestreo> query = em.createQuery("SELECT m FROM MetoMuestreo m WHERE m.metodo LIKE :name", MetoMuestreo.class).setParameter("name", name);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0).getIdMetMuestreo();
		else
			return -1L;	//Si el Metodo no existe se devuelve '-1' a modo de se�al de error
	}
	
	//Metodo que verifica si ya existe un Metodo con determinado nombre
	private boolean AlredyExists(MetoMuestreo method) throws ServiciosException {
		if(GetIDMethod(method.getMetodo()) == -1) 
			return false;
		else
			return true;
	}

}