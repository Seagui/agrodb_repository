package daos;

import java.util.List;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.Formulario;

@Local
public interface DAOFormulariosLocal {

	void CreateForm(Formulario form) throws ServiciosException;					//Metodo para Crear un Formulario
	void UpdateForm(Formulario form, String oldName) throws ServiciosException;	//Metodo para Modificar un Formulario
	List<Formulario> SelectForms();												//Metodo para obtener un List con todos los Formularios
	Long GetIDForm(String name);												//Metodo para obtener un Long con la ID de unn Formulario con determinado nombre
	
}