package daos;

import java.util.List;

import javax.ejb.Local;

import model.TipoInput;

@Local
public interface DAOTipoInputLocal {

	List<TipoInput> SelectInputs();	//Metodo para obtener un List con todos los Tipo Input

}
