package daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.Role;
import model.Usuario;

@Stateless
public class DAOUsuarios implements DAOUsuariosLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAOUsuarios() { }

	@Override
	public void CreateUser(Usuario user) throws ServiciosException {
		try {
			if(!AlredyExists(user)) {	//Antes de Crear el Usuario se verifica que no exista con otro ID
				em.persist(user);
				em.flush();
			}
			else {
				if (Discharged(user)) {
					user.setActivo("true");
					user.setIdUsuario(GetIDUser(user.getNombUsuario()));
					UpdateUser(user, user.getNombUsuario());
				} else {
					throw new ServiciosException("Ya existe un usuario con ese nombre de usuario");
				}
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear el Usuario: " + user.getNombre()); }
	}

	@Override
	public void UpdateUser(Usuario user, String oldUser) throws ServiciosException {
		 try {			 
			if (!user.getNombUsuario().equals(oldUser)) { //Si cambio el nombre de usuario
				if(!AlredyExists(user)) {	//Antes de Modificar el Usuario se verifica que no exista con otro ID
					em.merge(user);
					em.flush();
				} 
				else
					if (Discharged(user)) {
						user.setActivo("true");
						user.setIdUsuario(GetIDUser(user.getNombUsuario()));
						UpdateUser(user, user.getNombUsuario());
					} else {
						throw new ServiciosException("Ya existe un usuario con ese nombre de usuario");
					}
			} else {
				em.merge(user);
				em.flush();
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo actualizar el Usuario: " + user.getNombre()); }
	}

	@Override
	public void DeleteUser(Long id) throws ServiciosException {
		 try {
			 Usuario user = em.find(Usuario.class, id);
			 user.setActivo("false");
			 em.merge(user);
			 em.flush();
		 } catch(PersistenceException e){ throw new ServiciosException("No se pudo eliminar el Usuario"); }
	}

	@Override
	public List<Usuario> SelectUsers() {
		TypedQuery<Usuario> query = em.createQuery("SELECT u FROM Usuario u WHERE u.activo != 'false'", Usuario.class);
		
		return query.getResultList();
	}

	@Override
	public List<Usuario> SelectUsers(String username, String name, String surname, Role rol) {
		TypedQuery<Usuario> query;
		if (!username.equals("No")) {
			query = em.createQuery("SELECT u FROM Usuario u WHERE u.nombUsuario = :username AND u.activo != 'false'", Usuario.class).setParameter("username", username);
		}
		else {
			if (!(rol == null)) {
				query = em.createQuery("SELECT u FROM Usuario u "
						+ "WHERE u.nombre LIKE CONCAT('%',:name,'%') "
						+ "AND u.apellido LIKE CONCAT('%',:surname,'%') "
						+ "AND u.role = :role AND u.activo != 'false'", Usuario.class).setParameter("name", name).setParameter("surname", surname).setParameter("role", rol);
			}
			else {
				query = em.createQuery("SELECT u FROM Usuario u WHERE u.nombre LIKE CONCAT('%',:name,'%') AND u.apellido LIKE CONCAT('%',:surname,'%') AND u.activo != 'false'", Usuario.class).setParameter("name", name).setParameter("surname", surname);
			}
		}

		return query.getResultList();
	}

	@Override
	public Long GetIDUser(String nombUsuario) {
		TypedQuery<Usuario> query = em.createQuery("SELECT u FROM Usuario u WHERE u.nombUsuario LIKE :nombUsuario", Usuario.class).setParameter("nombUsuario", nombUsuario);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0).getIdUsuario();
		else
			return -1L;	//Si el Usuario no existe se devuelve '-1' a modo de se�al de error
	}

	//Metodo para verificar que existe el usuario intentando logearse
	@Override
	public Usuario Login(String nombUsuario, String contrasenia) {
		TypedQuery<Usuario> query = em.createQuery("SELECT u FROM Usuario u WHERE u.nombUsuario LIKE :nombUsuario AND u.contrasenia LIKE :contrasenia AND u.activo != 'false'", Usuario.class).setParameter("nombUsuario", nombUsuario).setParameter("contrasenia", contrasenia);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0);
		else
			return null;
	}
	
	@Override
	public int GetCountAdmins(Role role) {
		TypedQuery<Usuario> query = em.createQuery("SELECT u FROM Usuario u WHERE u.role LIKE :role AND u.activo != 'false'", Usuario.class).setParameter("role", role);
		
		return query.getResultList().size();
	}

	//Metodo que verifica si ya existe un Usuario con determinado nombre de usuario
	private boolean AlredyExists(Usuario user) throws ServiciosException {
		if(GetIDUser(user.getNombUsuario()) == -1) 
			return false;
		else
			return true;
	}
	
	//Metodo que veerifica si el usuario fue dado de baja
	private boolean Discharged(Usuario user) {
		TypedQuery<Usuario> query = em.createQuery("SELECT u FROM Usuario u WHERE u.nombUsuario LIKE :nombUsuario AND u.activo = 'false'", Usuario.class).setParameter("nombUsuario", user.getNombUsuario());
		
		if(query.getResultList().size() > 0)
			return true;
		else
			return false;
	}
	
}