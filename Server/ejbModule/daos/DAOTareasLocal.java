package daos;

import java.util.List;

import javax.ejb.Local;

import model.Tarea;
import exceptions.ServiciosException;

@Local
public interface DAOTareasLocal {

	void CreateTask(Tarea task) throws ServiciosException;	//Metodo para Crear una Tarea
	void UpdateTask(Tarea task) throws ServiciosException;	//Metodo para Modificar una Tarea
	void DeleteTask(Long id) throws ServiciosException;		//Metodo para Eliminar una Tarea con un ID determinado
	List<Tarea> SelectTasks();								//Metodo para obtener un List con todas las Tareas
	List<Tarea> SelectTasks(String roleName);				//Metodo para obtener un List con las Tareas asociadas a un Rol con determinado nombre
	Long GetIDTask(String name);							//Metodo para obtener un Long con la ID de una Tarea con determinado nombre
	
}