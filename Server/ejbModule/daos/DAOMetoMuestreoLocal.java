package daos;

import java.util.List;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.MetoMuestreo;

@Local
public interface DAOMetoMuestreoLocal {

	void CreateMethod(MetoMuestreo method) throws ServiciosException;	//Metodo para Crear un Metodo de Muestreo
	List<MetoMuestreo> SelectMethods();									//Metodo para obtener un List con todas los Metodos de Muestreo
	Long GetIDMethod(String name);										//Metodo para obtener un Long con la ID de un Metodo de Muestreo
	
}