package daos;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.ActiCampo;
import model.Formulario;
import model.MetoMuestreo;
import model.Role;
import model.Usuario;

@Stateless
public class DAOActiCampo implements DAOActiCampoLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAOActiCampo() { }

	@Override
	public void CreateActivity(ActiCampo activity) throws ServiciosException {
		try {
			if(!AlredyExists(activity)) {	//Antes de Crear el Formulario se verifica que no exista con otra ID
				em.persist(activity);
				em.flush();
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear la Actividad de Campo"); } 
	}
	
	@Override
	public List<ActiCampo> SelectActivitys() {
		TypedQuery<ActiCampo> query = em.createNamedQuery("ActiCampo.findAll", ActiCampo.class);
		
		return query.getResultList();
	}
	
	@Override
	public List<ActiCampo> SelectActivitys(String form) {
		TypedQuery<ActiCampo> query = em.createQuery("SELECT a FROM ActiCampo a "
				+ "INNER JOIN a.formulario f "
				+ "WHERE f.nombFormulario = :form", ActiCampo.class).setParameter("form", form);
		
		return query.getResultList();
	}
	
	@Override
	public List<ActiCampo> SelectActivitys(String form, Usuario user) {
		TypedQuery<ActiCampo> query = em.createQuery("SELECT a FROM ActiCampo a "
				+ "INNER JOIN a.formulario f "
				+ "WHERE f.nombFormulario = :form "
				+ "AND a.usuario = :usuario", ActiCampo.class).setParameter("form", form).setParameter("usuario", user);
		
		return query.getResultList();
	}

	@Override
	public List<ActiCampo> SelectActivitys(MetoMuestreo method, Role rol, Date fechaInicio, Date fechaFin) {
		TypedQuery<ActiCampo> query;
		if (fechaInicio != null && fechaFin != null) {
			query = em.createQuery("SELECT a FROM ActiCampo a "
					+ "INNER JOIN a.metoMuestreo m "
					+ "INNER JOIN a.usuario u "
					+ "INNER JOIN u.role r "
					+ "WHERE m.metodo LIKE CONCAT('%',:method,'%') "
					+ "AND r.nombre LIKE CONCAT('%',:rol,'%') "
					+ "AND a.fecha >= :fechaInicio AND a.fecha <= :fechaFin",
					ActiCampo.class).setParameter("method", method.getMetodo()).setParameter("rol", rol.getNombre()).setParameter("fechaInicio", fechaInicio).setParameter("fechaFin", fechaFin);
		} 
		else {
			query = em.createQuery("SELECT a FROM ActiCampo a "
					+ "INNER JOIN a.metoMuestreo m "
					+ "INNER JOIN a.usuario u "
					+ "INNER JOIN u.role r "
					+ "WHERE m.metodo LIKE CONCAT('%',:method,'%') "
					+ "AND r.nombre LIKE CONCAT('%',:rol,'%') ",
					ActiCampo.class).setParameter("method", method.getMetodo()).setParameter("rol", rol.getNombre());
		}

		return query.getResultList();
	}
	
	@Override
	public ActiCampo SelectActivity(Date fecha, Usuario usuario, Formulario formulario) {
		TypedQuery<ActiCampo> query = em.createQuery("SELECT a FROM ActiCampo a WHERE a.fecha LIKE :fecha AND a.usuario LIKE :usuario AND a.formulario LIKE :formulario", ActiCampo.class).setParameter("fecha", fecha).setParameter("usuario", usuario).setParameter("formulario", formulario);
		
		return query.getResultList().get(0);
	}
	
	@Override
	public Long GetIDActivity(Date fecha, Usuario usuario, Formulario formulario) {
		TypedQuery<ActiCampo> query = em.createQuery("SELECT a FROM ActiCampo a WHERE a.fecha LIKE :fecha AND a.usuario LIKE :usuario AND a.formulario LIKE :formulario", ActiCampo.class).setParameter("fecha", fecha).setParameter("usuario", usuario).setParameter("formulario", formulario);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0).getIdActiCampo();
		else
			return -1L;	//Si el Formulario no existe se devuelve '-1' a modo de se�al de error
	}

	//Metodo que verifica si ya existe un Formulario con determinado nombre
	private boolean AlredyExists(ActiCampo activity) throws ServiciosException {
		if(GetIDActivity(activity.getFecha(), activity.getUsuario(), activity.getFormulario()) == -1) 
			return false;
		else
			return true;
	}

}