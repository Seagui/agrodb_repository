package daos;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.Casilla;
import model.FormCasi;
import model.Formulario;

@Stateless
public class DAOFormCasi implements DAOFormCasiLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAOFormCasi() { }

	@Override
	public void CreateFormBox(FormCasi formBox) throws ServiciosException {
		try {
			em.persist(formBox);
			em.flush();
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear la relacion Formulario/Casilla"); } 
	}

	@Override
	public void DeleteFormBox(Long id) throws ServiciosException {		
		 try {
			 FormCasi formBox = em.find(FormCasi.class, id);
			 
			 em.remove(formBox);
			 em.flush();
		 } catch(PersistenceException e) { throw new ServiciosException("No se pudo eliminar el la relacion Formulario/Casilla"); } 
	}

	@Override
	public Long GetIDRoleTask(Formulario form, Casilla box) {
		TypedQuery<FormCasi> query = em.createQuery("SELECT fc FROM FormCasi fc WHERE fc.casilla LIKE :casilla AND fc.formulario LIKE :formulario", FormCasi.class).setParameter("casilla", box).setParameter("formulario", form);
		
		return query.getResultList().get(0).getIdFormCasi();
	}

}