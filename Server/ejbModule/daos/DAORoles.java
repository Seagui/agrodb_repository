package daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.Role;

@Stateless
public class DAORoles implements DAORolesLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAORoles() { }

	@Override
	public void CreateRole(Role role) throws ServiciosException {
		try {
			if(!AlredyExists(role.getNombre())) {	//Antes de Crear el Rol se verifica que no exista con otra ID
				em.persist(role);
				em.flush();
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear el Rol: " + role.getNombre()); } 
	}

	@Override
	public void UpdateRole(Role role) throws ServiciosException {
		 try {
			if(!AlredyExists(role.getNombre())) {	//Antes de Modificar el Rol se verifica que no exista con otra ID
				em.merge(role);
				em.flush();
			}
			else
				throw new ServiciosException("El rol ya existe");
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo actualizar el Rol: " + role.getNombre()); }
	}

	@Override
	public void DeleteRole(Long id) throws ServiciosException {
		 try {
			 Role role = em.find(Role.class, id);
			 
			 em.remove(role);
			 em.flush();
		 } catch(PersistenceException e) { throw new ServiciosException("No se pudo eliminar el Rol"); } 
	}

	@Override
	public List<Role> SelectRoles() {
		TypedQuery<Role> query = em.createNamedQuery("Role.findAll", Role.class);
		
		return query.getResultList();
	}

	@Override
	public Long GetIDRole(String name) {
		TypedQuery<Role> query = em.createQuery("SELECT r FROM Role r WHERE r.nombre LIKE :name", Role.class).setParameter("name", name);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0).getIdRol();
		else
			return -1L;	//Si el Rol no existe se devuelve '-1' a modo de se�al de error
	}
	
	//Metodo que verifica si ya existe un Rol con determinado nombre
	private boolean AlredyExists(String roleName) throws ServiciosException {
		if(GetIDRole(roleName) == -1) 
			return false;
		else
			return true;
	}

}