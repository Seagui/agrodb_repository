package daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.ActiCampo;
import model.Casilla;
import model.Dato;

@Stateless
public class DAODatos implements DAODatosLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAODatos() { }

	@Override
	public void CreateData(Dato data) throws ServiciosException {
		try {
			if(!AlredyExists(data)) {	//Antes de Crear el Dato se verifica que no exista con otra ID
				em.persist(data);
				em.flush();
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear el dato"); } 
	}

	@Override
	public Long GetIDData(ActiCampo activity, Casilla box, String valor) {
		TypedQuery<Dato> query = em.createQuery("SELECT d FROM Dato d WHERE d.actiCampo = :activity "
				+ "AND d.casilla = :box "
				+ "AND d.valor = :valor", Dato.class).setParameter("activity", activity).setParameter("box", box).setParameter("valor", valor);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0).getIdDato();
		else
			return -1L;	//Si el Dato no existe se devuelve '-1' a modo de se�al de error
	}

	//Metodo que verifica si ya existe una Casilla con determinado nombre
	private boolean AlredyExists(Dato data) throws ServiciosException {
		if(GetIDData(data.getActiCampo(), data.getCasilla(), data.getValor()) == -1) 
			return false;
		else
			return true;
	}

	@Override
	public List<Dato> SelectData(ActiCampo activity) {
		TypedQuery<Dato> query = em.createQuery("SELECT d FROM Dato d "
				+ "WHERE d.actiCampo = :activity", Dato.class).setParameter("activity", activity);
		
		return query.getResultList();
	}
	
}