package daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.Formulario;

/**
 * Session Bean implementation class FormulariosBean
 */
@Stateless
public class DAOFormularios implements DAOFormulariosLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAOFormularios() { }

	@Override
	public void CreateForm(Formulario form) throws ServiciosException {
		try {
			if(!AlredyExists(form)) {	//Antes de Crear el Formulario se verifica que no exista con otra ID
				em.persist(form);
				em.flush();
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear el Formulario: " + form.getNombFormulario()); } 
	}

	@Override
	public void UpdateForm(Formulario form, String oldName) throws ServiciosException {
		 try{
			if (!form.getNombFormulario().equals(oldName)) { //Si cambio el nombre de Formulario
				if(!AlredyExists(form)) {	//Antes de Modificar el Formulario se verifica que no exista con otro ID
					em.merge(form);
					em.flush();
				} 
				else
					throw new ServiciosException("Ya existe un formulario con ese nombre");
			} else {
				em.merge(form);
				em.flush();
			}
		} catch(PersistenceException e){ throw new ServiciosException("No se pudo actualizar el Formulario: " + form.getNombFormulario()); }
	}

	@Override
	public List<Formulario> SelectForms() {
		TypedQuery<Formulario> query = em.createNamedQuery("Formulario.findAll", Formulario.class);
		
		return query.getResultList();
	}

	@Override
	public Long GetIDForm(String name) {
		TypedQuery<Formulario> query = em.createQuery("SELECT f FROM Formulario f WHERE f.nombFormulario LIKE :name", Formulario.class).setParameter("name", name);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0).getIdFormulario();
		else
			return -1L;	//Si el Formulario no existe se devuelve '-1' a modo de se�al de error
	}

	//Metodo que verifica si ya existe un Formulario con determinado nombre
	private boolean AlredyExists(Formulario form) throws ServiciosException {
		if(GetIDForm(form.getNombFormulario()) == -1) 
			return false;
		else
			return true;
	}
	
}