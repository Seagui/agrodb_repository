package daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import model.TipoInput;

@Stateless
public class DAOTipoInput implements DAOTipoInputLocal {
	
	@PersistenceContext
	private EntityManager em;

    public DAOTipoInput() {}    

	@Override
   	public List<TipoInput> SelectInputs() {
		TypedQuery<TipoInput> query = em.createNamedQuery("TipoInput.findAll", TipoInput.class);
		
		return query.getResultList();
   	}
    
}
