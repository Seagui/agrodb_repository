package daos;

import java.util.List;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.Zona;

@Local
public interface DAOZonaLocal {

	void CreateZone(Zona zone) throws ServiciosException;	//Metodo para Crear una Zona
	List<Zona> SelectZones();								//Metodo para obtener un List con todas las Zonas
	Long GetIDZone(String name);							//Metodo para obtener un Long con la ID de una Zona
	
}