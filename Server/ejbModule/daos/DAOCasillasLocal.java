package daos;

import java.util.List;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.Casilla;

@Local
public interface DAOCasillasLocal {

	void CreateBox(Casilla box) throws ServiciosException;	//Metodo para Crear una Casilla
	List<Casilla> SelectBoxs();								//Metodo para obtener un List con todas las Casillas
	List<Casilla> SelectBoxs(String formName);				//Metodo para obtener un List con las Casillas asociadas a un Formulario con determinado nombre
	Long GetIDBox(String name);								//Metodo para obtener un Long con la ID de unn Formulario con determinado nombre
	
}