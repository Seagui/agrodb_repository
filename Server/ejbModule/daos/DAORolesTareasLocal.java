package daos;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.Role;
import model.RolesTarea;
import model.Tarea;

@Local
public interface DAORolesTareasLocal {

	void CreateRoleTask(RolesTarea roleTask) throws ServiciosException;	//Metodo para Crear una Relacion Rol/Tarea
	void DeleteRoleTask(Long id) throws ServiciosException;				//Metodo para Eliminar una Relacion Rol/Tarea
	Long GetIDRoleTask(Tarea task, Role role);							//Metodo para obtener un Long con la ID de una Relacion Rol/Tarea con un determinado Rol y una determinada Tarea
	int GetCountTasks(Tarea task);										//Metodo para obtener la cantidad de relaciones en la que esta una Tarea
	
}