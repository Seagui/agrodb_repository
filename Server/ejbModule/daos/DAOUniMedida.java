package daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.UnidMedida;

@Stateless
public class DAOUniMedida implements DAOUniMedidaLocal {

	@PersistenceContext
	private EntityManager em;
   
    public DAOUniMedida() { }

	@Override
	public void CreateUnit(UnidMedida unit) throws ServiciosException {
		try {
			if(!AlredyExists(unit)) {	//Antes de Crear la Unidad de Medida se verifica que no exista con otra ID
				em.persist(unit);
				em.flush();
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear la Unidad de Medida: " + unit.getNombre()); } 
	}

	@Override
	public List<UnidMedida> SelectUnits() {
		TypedQuery<UnidMedida> query = em.createNamedQuery("UnidMedida.findAll", UnidMedida.class);
		
		return query.getResultList();
	}

	@Override
	public Long GetIDUnit(String name) {
		TypedQuery<UnidMedida> query = em.createQuery("SELECT u FROM UnidMedida u WHERE u.nombre LIKE :name", UnidMedida.class).setParameter("name", name);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0).getIdUniMedida();
		else
			return -1L;	//Si la Unidad no existe se devuelve '-1' a modo de se�al de error
	}

	//Metodo que verifica si ya existe una Unidad con determinado nombre
	private boolean AlredyExists(UnidMedida unit) throws ServiciosException {
		if(GetIDUnit(unit.getNombre()) == -1) 
			return false;
		else
			return true;
	}
	
}