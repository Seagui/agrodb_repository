package daos;

import java.util.List;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.UnidMedida;

@Local
public interface DAOUniMedidaLocal {

	void CreateUnit(UnidMedida unit) throws ServiciosException;	//Metodo para Crear una Unidad de Medida
	List<UnidMedida> SelectUnits();								//Metodo para obtener un List con todas las Unidades de Medida
	Long GetIDUnit(String name);								//Metodo para obtener un Long con la ID de una Unidad de Medida

}