package daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.Zona;

@Stateless
public class DAOZona implements DAOZonaLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAOZona() { }

	@Override
	public void CreateZone(Zona zone) throws ServiciosException {
		try {
			if(!AlredyExists(zone)) {	//Antes de Crear la Zona se verifica que no exista con otra ID
				em.persist(zone);
				em.flush();
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear la Zona: " + zone.getZona()); } 
	}

	@Override
	public List<Zona> SelectZones() {
		TypedQuery<Zona> query = em.createNamedQuery("Zona.findAll", Zona.class);
		
		return query.getResultList();	}

	@Override
	public Long GetIDZone(String name) {
		TypedQuery<Zona> query = em.createQuery("SELECT z FROM Zona z WHERE z.zona LIKE :name", Zona.class).setParameter("name", name);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0).getIdZona();
		else
			return -1L;	//Si la Estacion no existe se devuelve '-1' a modo de se�al de error
	}

	//Metodo que verifica si ya existe una Zona con determinado nombre
	private boolean AlredyExists(Zona zone) throws ServiciosException {
		if(GetIDZone(zone.getZona()) == -1) 
			return false;
		else
			return true;
	}
	
}