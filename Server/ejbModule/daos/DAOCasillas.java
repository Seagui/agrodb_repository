package daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.Casilla;

/**
 * Session Bean implementation class CasillasBean
 */
@Stateless
public class DAOCasillas implements DAOCasillasLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAOCasillas() { }

	@Override
	public void CreateBox(Casilla box) throws ServiciosException {
		try {
			if(!AlredyExists(box)) {	//Antes de Crear la Casilla se verifica que no exista con otra ID
				em.persist(box);
				em.flush();
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear la Casilla: " + box.getParametro()); } 
	}

	@Override
	public List<Casilla> SelectBoxs() {
		TypedQuery<Casilla> query = em.createNamedQuery("Casilla.findAll", Casilla.class);
		
		return query.getResultList();
	}

	@Override
	public Long GetIDBox(String name) {
		TypedQuery<Casilla> query = em.createQuery("SELECT c FROM Casilla c WHERE c.parametro LIKE :name", Casilla.class).setParameter("name", name);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0).getIdCasilla();
		else
			return -1L;	//Si la Casilla no existe se devuelve '-1' a modo de se�al de error
	}
	
	//Metodo que verifica si ya existe una Casilla con determinado nombre
	private boolean AlredyExists(Casilla box) throws ServiciosException {
		if(GetIDBox(box.getParametro()) == -1) 
			return false;
		else
			return true;
	}

	@Override
	public List<Casilla> SelectBoxs(String formName) {
		TypedQuery<Casilla> query = em.createQuery("SELECT c FROM Casilla c "
				+ "INNER JOIN c.formCasis fc "
				+ "INNER JOIN fc.formulario f "
				+ "WHERE f.nombFormulario = :formName", Casilla.class).setParameter("formName", formName);
		
		return query.getResultList();
	}
	
}