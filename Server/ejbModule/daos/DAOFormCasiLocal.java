package daos;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.Casilla;
import model.FormCasi;
import model.Formulario;

@Local
public interface DAOFormCasiLocal {

	void CreateFormBox(FormCasi formBox) throws ServiciosException;	//Metodo para Crear una Relacion Formulario/Casilla
	void DeleteFormBox(Long id) throws ServiciosException;			//Metodo para Eliminar una Relacion Formulario/Casilla
	Long GetIDRoleTask(Formulario form, Casilla box);				//Metodo para obtener un Long con la ID de una Relacion Formulario/Casilla
	
}