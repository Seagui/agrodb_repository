package daos;

import java.util.List;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.EstaMuestreo;

@Local
public interface DAOEstaMuestreoLocal {

	void CreateStation(EstaMuestreo station) throws ServiciosException;	//Metodo para Crear una Estacion
	List<EstaMuestreo> SelectStation();									//Metodo para obtener un List con todas las Estaciones de Muestreo
	Long GetIDStation(String name);										//Metodo para obtener un Long con la ID de una Estacion de Muestreo
	
}