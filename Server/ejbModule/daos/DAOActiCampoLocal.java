package daos;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.ActiCampo;
import model.Formulario;
import model.MetoMuestreo;
import model.Role;
import model.Usuario;

@Local
public interface DAOActiCampoLocal {

	void CreateActivity(ActiCampo activity) throws ServiciosException;									//Metodo para Crear una Actividad de Campo
	List<ActiCampo> SelectActivitys();																	//Metodo para obtener un List con todas las Actividades de Campo
	List<ActiCampo> SelectActivitys(String form);														//Metodo para obtener un List con todas las Actividades de Campo de un Formulario
	List<ActiCampo> SelectActivitys(String form, Usuario user);											//Metodo para obtener un List con todas las Actividades de Campo de un Formulario y Usuario determinado
	List<ActiCampo> SelectActivitys(MetoMuestreo method, Role rol, Date fechaInicio, Date fechaFin);	//Metodo para obtener un List con los Usuarios que corresponden a un Filtro determinado
	Long GetIDActivity(Date fecha, Usuario usuario, Formulario formulario);								//Metodo para obtener un Long con la ID de una Actividad de Campo determinada
	ActiCampo SelectActivity(Date fecha, Usuario usuario, Formulario formulario);						//Metodo para obtener una Actividad determinada
	
}