package daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.EstaMuestreo;

@Stateless
public class DAOEstaMuestreo implements DAOEstaMuestreoLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAOEstaMuestreo() { }

	@Override
	public void CreateStation(EstaMuestreo station) throws ServiciosException {
		try {
			if(!AlredyExists(station)) {	//Antes de Crear la Estacion de Muestreo se verifica que no exista con otra ID
				em.persist(station);
				em.flush();
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear la Estacion de Muestreo: " + station.getEstacion()); } 
	}

	@Override
	public List<EstaMuestreo> SelectStation() {
		TypedQuery<EstaMuestreo> query = em.createNamedQuery("EstaMuestreo.findAll", EstaMuestreo.class);
		
		return query.getResultList();
	}

	@Override
	public Long GetIDStation(String name) {
		TypedQuery<EstaMuestreo> query = em.createQuery("SELECT e FROM EstaMuestreo e WHERE e.estacion LIKE :name", EstaMuestreo.class).setParameter("name", name);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0).getIdEstMuestreo();
		else
			return -1L;	//Si la Estacion no existe se devuelve '-1' a modo de se�al de error
	}
	
	//Metodo que verifica si ya existe una Estacion con determinado nombre
	private boolean AlredyExists(EstaMuestreo station) throws ServiciosException {
		if(GetIDStation(station.getEstacion()) == -1) 
			return false;
		else
			return true;
	}

}