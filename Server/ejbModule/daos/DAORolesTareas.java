package daos;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.Role;
import model.RolesTarea;
import model.Tarea;

@Stateless
public class DAORolesTareas implements DAORolesTareasLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAORolesTareas() { }

	@Override
	public void CreateRoleTask(RolesTarea roleTask) throws ServiciosException {
		try {
			em.persist(roleTask);
			em.flush();
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear la relacion Rol/Tarea"); } 
	}

	@Override
	public void DeleteRoleTask(Long id) throws ServiciosException {
		 try {
			 RolesTarea roleTask = em.find(RolesTarea.class, id);
			 
			 em.remove(roleTask);
			 em.flush();
		 } catch(PersistenceException e) { throw new ServiciosException("No se pudo eliminar el la relacion Rol/Tarea"); } 
	}
	
	@Override
	public Long GetIDRoleTask(Tarea task, Role role) {
		TypedQuery<RolesTarea> query = em.createQuery("SELECT rt FROM RolesTarea rt WHERE rt.tarea LIKE :task AND rt.role LIKE :role", RolesTarea.class).setParameter("task", task).setParameter("role", role);
		
		return query.getResultList().get(0).getIdRolTarea();
	}
	
	@Override
	public int GetCountTasks(Tarea task) {
		TypedQuery<RolesTarea> query = em.createQuery("SELECT rt FROM RolesTarea rt WHERE rt.tarea LIKE :task", RolesTarea.class).setParameter("task", task);
		
		return query.getResultList().size();
	}
    
}
