package daos;

import java.util.List;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.ActiCampo;
import model.Casilla;
import model.Dato;

@Local
public interface DAODatosLocal {

	List<Dato> SelectData(ActiCampo activity);						//Metodo para obtener datos de una Actividad
	void CreateData(Dato data) throws ServiciosException;			//Metodo para Crear un Dato
	Long GetIDData(ActiCampo activity, Casilla box, String valor);	//Metodo para obtener un Long con la ID de un Dato determinado
	
}