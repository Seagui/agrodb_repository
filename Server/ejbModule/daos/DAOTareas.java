package daos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import exceptions.ServiciosException;
import model.Tarea;

@Stateless
public class DAOTareas implements DAOTareasLocal {

	@PersistenceContext
	private EntityManager em;
	
    public DAOTareas() { }

	@Override
	public void CreateTask(Tarea task) throws ServiciosException {
		try {
			if(!AlredyExists(task)) {	//Antes de Crear la Tarea se verifica que no exista con otra ID
				em.persist(task);
				em.flush();
			}
		} catch(PersistenceException e) { throw new ServiciosException("No se pudo crear la Tarea: " + task.getNombre()); } 
	}

	@Override
	public void UpdateTask(Tarea task) throws ServiciosException {
		 try{
			if(!AlredyExists(task)) {	//Antes de Modificar la Tarea se verifica que no exista con otra ID
				em.merge(task);
				em.flush();
			} 
			else
				throw new ServiciosException("La Tarea ya existe");
		} catch(PersistenceException e){ throw new ServiciosException("No se pudo actualizar la Tarea: " + task.getNombre()); }
	}

	@Override
	public void DeleteTask(Long id) throws ServiciosException {
		 try {
			 Tarea task = em.find(Tarea.class, id);
			 
			 em.remove(task);
			 em.flush();
		 } catch(PersistenceException e){
				throw new ServiciosException("No se pudo eliminar la Tarea");
		 } 
	}

	@Override
	public List<Tarea> SelectTasks() {
		TypedQuery<Tarea> query = em.createNamedQuery("Tarea.findAll", Tarea.class);
		
		return query.getResultList();
	}

	@Override
	public List<Tarea> SelectTasks(String roleName) {
		TypedQuery<Tarea> query = em.createQuery("SELECT t FROM Tarea t "
				+ "INNER JOIN t.rolesTareas rt "
				+ "INNER JOIN rt.role r "
				+ "WHERE r.nombre = :roleName", Tarea.class).setParameter("roleName", roleName);
		
		return query.getResultList();
	}
	
	@Override
	public Long GetIDTask(String name) {
		TypedQuery<Tarea> query = em.createQuery("SELECT t FROM Tarea t WHERE t.nombre LIKE :name", Tarea.class).setParameter("name", name);
		
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0).getIdTarea();
		else
			return -1L;	//Si la Tarea no existe se devuelve '-1' a modo de se�al de error
	}
	
	//Metodo que verifica si ya existe una Tarea con determinado nombre
	private boolean AlredyExists(Tarea task) throws ServiciosException {
		if(GetIDTask(task.getNombre()) == -1) 
			return false;
		else
			return true;
	}

}