package daos;

import java.util.List;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.Role;

@Local
public interface DAORolesLocal {

	void CreateRole(Role role) throws ServiciosException;	//Metodo para Crear un Rol
	void UpdateRole(Role role) throws ServiciosException;	//Metodo para Modificar un Rol
	void DeleteRole(Long id) throws ServiciosException;		//Metodo para Eliminar un Rol con un ID determinado
	List<Role> SelectRoles();								//Metodo para obtener un List con todos los Roles
	Long GetIDRole(String name);							//Metodo para obtener un Long con la ID de un Rol con determinado nombre
	
}