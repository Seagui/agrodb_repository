package daos;

import java.util.List;

import javax.ejb.Local;

import exceptions.ServiciosException;
import model.Role;
import model.Usuario;

@Local
public interface DAOUsuariosLocal {

	void CreateUser(Usuario user) throws ServiciosException;							//Metodo para Crear un Usuario
	void UpdateUser(Usuario user, String oldUser) throws ServiciosException;			//Metodo para Modificar un Usuario
	void DeleteUser(Long id) throws ServiciosException;									//Metodo para Eliminar un Usuario con un ID determinado
	List<Usuario> SelectUsers();														//Metodo para obtener un List con todos los Usuarios
	List<Usuario> SelectUsers(String username, String name, String surname, Role rol);	//Metodo para obtener un List con los Usuarios que corresponden a un Filtro determinado
	Long GetIDUser(String nombUsuario);													//Metodo para obtener un Long con la ID de un Usuario con determinado nombre de usuario
	Usuario Login(String nombUsuario, String contrasenia);								//Metodo para verificar un usuario en base a su contraseņa y nombre de usuario
	int GetCountAdmins(Role role);														//Metodo para obtener la cantidad de usuarios admins
	
}