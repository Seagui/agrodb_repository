package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the METO_MUESTREO database table.
 * 
 */
@Entity
@Table(name="METO_MUESTREO")
@NamedQuery(name="MetoMuestreo.findAll", query="SELECT m FROM MetoMuestreo m")
public class MetoMuestreo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="METO_MUESTREO_IDMETMUESTREO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="METO_MUESTREO_IDMETMUESTREO_GENERATOR")
	@Column(name="ID_MET_MUESTREO")
	private long idMetMuestreo;

	private String metodo;

	//bi-directional many-to-one association to ActiCampo
	@OneToMany(mappedBy="metoMuestreo")
	private List<ActiCampo> actiCampos;

	public MetoMuestreo() {
	}

	public long getIdMetMuestreo() {
		return this.idMetMuestreo;
	}

	public void setIdMetMuestreo(long idMetMuestreo) {
		this.idMetMuestreo = idMetMuestreo;
	}

	public String getMetodo() {
		return this.metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public List<ActiCampo> getActiCampos() {
		return this.actiCampos;
	}

	public void setActiCampos(List<ActiCampo> actiCampos) {
		this.actiCampos = actiCampos;
	}

	public ActiCampo addActiCampo(ActiCampo actiCampo) {
		getActiCampos().add(actiCampo);
		actiCampo.setMetoMuestreo(this);

		return actiCampo;
	}

	public ActiCampo removeActiCampo(ActiCampo actiCampo) {
		getActiCampos().remove(actiCampo);
		actiCampo.setMetoMuestreo(null);

		return actiCampo;
	}

}