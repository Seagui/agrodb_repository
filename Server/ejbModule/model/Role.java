package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the "ROLES" database table.
 * 
 */
@Entity
@Table(name="\"ROLES\"")
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ROLES_IDROL_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ROLES_IDROL_GENERATOR")
	@Column(name="ID_ROL")
	private long idRol;

	private String nombre;

	//bi-directional many-to-one association to RolesTarea
	@OneToMany(mappedBy="role")
	private List<RolesTarea> rolesTareas;

	//bi-directional many-to-one association to Usuario
	@OneToMany(mappedBy="role")
	private List<Usuario> usuarios;

	public Role() {
	}

	public long getIdRol() {
		return this.idRol;
	}

	public void setIdRol(long idRol) {
		this.idRol = idRol;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<RolesTarea> getRolesTareas() {
		return this.rolesTareas;
	}

	public void setRolesTareas(List<RolesTarea> rolesTareas) {
		this.rolesTareas = rolesTareas;
	}

	public RolesTarea addRolesTarea(RolesTarea rolesTarea) {
		getRolesTareas().add(rolesTarea);
		rolesTarea.setRole(this);

		return rolesTarea;
	}

	public RolesTarea removeRolesTarea(RolesTarea rolesTarea) {
		getRolesTareas().remove(rolesTarea);
		rolesTarea.setRole(null);

		return rolesTarea;
	}

	public List<Usuario> getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Usuario addUsuario(Usuario usuario) {
		getUsuarios().add(usuario);
		usuario.setRole(this);

		return usuario;
	}

	public Usuario removeUsuario(Usuario usuario) {
		getUsuarios().remove(usuario);
		usuario.setRole(null);

		return usuario;
	}

}