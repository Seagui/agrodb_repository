package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ESTA_MUESTREO database table.
 * 
 */
@Entity
@Table(name="ESTA_MUESTREO")
@NamedQuery(name="EstaMuestreo.findAll", query="SELECT e FROM EstaMuestreo e")
public class EstaMuestreo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ESTA_MUESTREO_IDESTMUESTREO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ESTA_MUESTREO_IDESTMUESTREO_GENERATOR")
	@Column(name="ID_EST_MUESTREO")
	private long idEstMuestreo;

	private String estacion;

	//bi-directional many-to-one association to ActiCampo
	@OneToMany(mappedBy="estaMuestreo")
	private List<ActiCampo> actiCampos;

	public EstaMuestreo() {
	}

	public long getIdEstMuestreo() {
		return this.idEstMuestreo;
	}

	public void setIdEstMuestreo(long idEstMuestreo) {
		this.idEstMuestreo = idEstMuestreo;
	}

	public String getEstacion() {
		return this.estacion;
	}

	public void setEstacion(String estacion) {
		this.estacion = estacion;
	}

	public List<ActiCampo> getActiCampos() {
		return this.actiCampos;
	}

	public void setActiCampos(List<ActiCampo> actiCampos) {
		this.actiCampos = actiCampos;
	}

	public ActiCampo addActiCampo(ActiCampo actiCampo) {
		getActiCampos().add(actiCampo);
		actiCampo.setEstaMuestreo(this);

		return actiCampo;
	}

	public ActiCampo removeActiCampo(ActiCampo actiCampo) {
		getActiCampos().remove(actiCampo);
		actiCampo.setEstaMuestreo(null);

		return actiCampo;
	}

}