package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the TIPO_INPUT database table.
 * 
 */
@Entity
@Table(name="TIPO_INPUT")
@NamedQuery(name="TipoInput.findAll", query="SELECT t FROM TipoInput t")
public class TipoInput implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TIPO_INPUT_IDTIPINPUT_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TIPO_INPUT_IDTIPINPUT_GENERATOR")
	@Column(name="ID_TIP_INPUT")
	private long idTipInput;

	private String nombre;

	//bi-directional many-to-one association to Casilla
	@OneToMany(mappedBy="tipoInput")
	private List<Casilla> casillas;

	public TipoInput() {
	}

	public long getIdTipInput() {
		return this.idTipInput;
	}

	public void setIdTipInput(long idTipInput) {
		this.idTipInput = idTipInput;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Casilla> getCasillas() {
		return this.casillas;
	}

	public void setCasillas(List<Casilla> casillas) {
		this.casillas = casillas;
	}

	public Casilla addCasilla(Casilla casilla) {
		getCasillas().add(casilla);
		casilla.setTipoInput(this);

		return casilla;
	}

	public Casilla removeCasilla(Casilla casilla) {
		getCasillas().remove(casilla);
		casilla.setTipoInput(null);

		return casilla;
	}

}