package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the FORMULARIOS database table.
 * 
 */
@Entity
@Table(name="FORMULARIOS")
@NamedQuery(name="Formulario.findAll", query="SELECT f FROM Formulario f")
public class Formulario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FORMULARIOS_IDFORMULARIO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FORMULARIOS_IDFORMULARIO_GENERATOR")
	@Column(name="ID_FORMULARIO")
	private long idFormulario;

	private String activo;

	@Column(name="NOMB_FORMULARIO")
	private String nombFormulario;

	@Lob
	private String resumen;

	//bi-directional many-to-one association to ActiCampo
	@OneToMany(mappedBy="formulario")
	private List<ActiCampo> actiCampos;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="ID_USUARIO")
	private Usuario usuario;

	//bi-directional many-to-one association to FormCasi
	@OneToMany(mappedBy="formulario")
	private List<FormCasi> formCasis;

	public Formulario() {
	}

	public long getIdFormulario() {
		return this.idFormulario;
	}

	public void setIdFormulario(long idFormulario) {
		this.idFormulario = idFormulario;
	}

	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getNombFormulario() {
		return this.nombFormulario;
	}

	public void setNombFormulario(String nombFormulario) {
		this.nombFormulario = nombFormulario;
	}

	public String getResumen() {
		return this.resumen;
	}

	public void setResumen(String resumen) {
		this.resumen = resumen;
	}

	public List<ActiCampo> getActiCampos() {
		return this.actiCampos;
	}

	public void setActiCampos(List<ActiCampo> actiCampos) {
		this.actiCampos = actiCampos;
	}

	public ActiCampo addActiCampo(ActiCampo actiCampo) {
		getActiCampos().add(actiCampo);
		actiCampo.setFormulario(this);

		return actiCampo;
	}

	public ActiCampo removeActiCampo(ActiCampo actiCampo) {
		getActiCampos().remove(actiCampo);
		actiCampo.setFormulario(null);

		return actiCampo;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<FormCasi> getFormCasis() {
		return this.formCasis;
	}

	public void setFormCasis(List<FormCasi> formCasis) {
		this.formCasis = formCasis;
	}

	public FormCasi addFormCasi(FormCasi formCasi) {
		getFormCasis().add(formCasi);
		formCasi.setFormulario(this);

		return formCasi;
	}

	public FormCasi removeFormCasi(FormCasi formCasi) {
		getFormCasis().remove(formCasi);
		formCasi.setFormulario(null);

		return formCasi;
	}

}