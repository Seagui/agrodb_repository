package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the CASILLAS database table.
 * 
 */
@Entity
@Table(name="CASILLAS")
@NamedQuery(name="Casilla.findAll", query="SELECT c FROM Casilla c")
public class Casilla implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CASILLAS_IDCASILLA_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CASILLAS_IDCASILLA_GENERATOR")
	@Column(name="ID_CASILLA")
	private long idCasilla;

	private String activo;

	private String descripcion;

	private String parametro;

	//bi-directional many-to-one association to TipoInput
	@ManyToOne
	@JoinColumn(name="ID_TIP_INPUT")
	private TipoInput tipoInput;

	//bi-directional many-to-one association to UnidMedida
	@ManyToOne
	@JoinColumn(name="ID_UNI_MEDIDA")
	private UnidMedida unidMedida;

	//bi-directional many-to-one association to Dato
	@OneToMany(mappedBy="casilla")
	private List<Dato> datos;

	//bi-directional many-to-one association to FormCasi
	@OneToMany(mappedBy="casilla")
	private List<FormCasi> formCasis;

	public Casilla() {
	}

	public long getIdCasilla() {
		return this.idCasilla;
	}

	public void setIdCasilla(long idCasilla) {
		this.idCasilla = idCasilla;
	}

	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getParametro() {
		return this.parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	public TipoInput getTipoInput() {
		return this.tipoInput;
	}

	public void setTipoInput(TipoInput tipoInput) {
		this.tipoInput = tipoInput;
	}

	public UnidMedida getUnidMedida() {
		return this.unidMedida;
	}

	public void setUnidMedida(UnidMedida unidMedida) {
		this.unidMedida = unidMedida;
	}

	public List<Dato> getDatos() {
		return this.datos;
	}

	public void setDatos(List<Dato> datos) {
		this.datos = datos;
	}

	public Dato addDato(Dato dato) {
		getDatos().add(dato);
		dato.setCasilla(this);

		return dato;
	}

	public Dato removeDato(Dato dato) {
		getDatos().remove(dato);
		dato.setCasilla(null);

		return dato;
	}

	public List<FormCasi> getFormCasis() {
		return this.formCasis;
	}

	public void setFormCasis(List<FormCasi> formCasis) {
		this.formCasis = formCasis;
	}

	public FormCasi addFormCasi(FormCasi formCasi) {
		getFormCasis().add(formCasi);
		formCasi.setCasilla(this);

		return formCasi;
	}

	public FormCasi removeFormCasi(FormCasi formCasi) {
		getFormCasis().remove(formCasi);
		formCasi.setCasilla(null);

		return formCasi;
	}

}