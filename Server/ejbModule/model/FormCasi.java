package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FORM_CASI database table.
 * 
 */
@Entity
@Table(name="FORM_CASI")
@NamedQuery(name="FormCasi.findAll", query="SELECT f FROM FormCasi f")
public class FormCasi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FORM_CASI_IDFORMCASI_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FORM_CASI_IDFORMCASI_GENERATOR")
	@Column(name="ID_FORM_CASI")
	private long idFormCasi;

	//bi-directional many-to-one association to Casilla
	@ManyToOne
	@JoinColumn(name="ID_CASILLA")
	private Casilla casilla;

	//bi-directional many-to-one association to Formulario
	@ManyToOne
	@JoinColumn(name="ID_FORMULARIO")
	private Formulario formulario;

	public FormCasi() {
	}

	public long getIdFormCasi() {
		return this.idFormCasi;
	}

	public void setIdFormCasi(long idFormCasi) {
		this.idFormCasi = idFormCasi;
	}

	public Casilla getCasilla() {
		return this.casilla;
	}

	public void setCasilla(Casilla casilla) {
		this.casilla = casilla;
	}

	public Formulario getFormulario() {
		return this.formulario;
	}

	public void setFormulario(Formulario formulario) {
		this.formulario = formulario;
	}

}