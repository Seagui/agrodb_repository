package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ZONAS database table.
 * 
 */
@Entity
@Table(name="ZONAS")
@NamedQuery(name="Zona.findAll", query="SELECT z FROM Zona z")
public class Zona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ZONAS_IDZONA_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ZONAS_IDZONA_GENERATOR")
	@Column(name="ID_ZONA")
	private long idZona;

	private String zona;

	//bi-directional many-to-one association to ActiCampo
	@OneToMany(mappedBy="zona")
	private List<ActiCampo> actiCampos;

	//bi-directional many-to-one association to Localidade
	@ManyToOne
	@JoinColumn(name="ID_LOCALIDAD")
	private Localidade localidade;

	public Zona() {
	}

	public long getIdZona() {
		return this.idZona;
	}

	public void setIdZona(long idZona) {
		this.idZona = idZona;
	}

	public String getZona() {
		return this.zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public List<ActiCampo> getActiCampos() {
		return this.actiCampos;
	}

	public void setActiCampos(List<ActiCampo> actiCampos) {
		this.actiCampos = actiCampos;
	}

	public ActiCampo addActiCampo(ActiCampo actiCampo) {
		getActiCampos().add(actiCampo);
		actiCampo.setZona(this);

		return actiCampo;
	}

	public ActiCampo removeActiCampo(ActiCampo actiCampo) {
		getActiCampos().remove(actiCampo);
		actiCampo.setZona(null);

		return actiCampo;
	}

	public Localidade getLocalidade() {
		return this.localidade;
	}

	public void setLocalidade(Localidade localidade) {
		this.localidade = localidade;
	}

}