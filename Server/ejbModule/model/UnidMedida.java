package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the UNID_MEDIDA database table.
 * 
 */
@Entity
@Table(name="UNID_MEDIDA")
@NamedQuery(name="UnidMedida.findAll", query="SELECT u FROM UnidMedida u")
public class UnidMedida implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="UNID_MEDIDA_IDUNIMEDIDA_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UNID_MEDIDA_IDUNIMEDIDA_GENERATOR")
	@Column(name="ID_UNI_MEDIDA")
	private long idUniMedida;

	private String nombre;

	//bi-directional many-to-one association to Casilla
	@OneToMany(mappedBy="unidMedida")
	private List<Casilla> casillas;

	public UnidMedida() {
	}

	public long getIdUniMedida() {
		return this.idUniMedida;
	}

	public void setIdUniMedida(long idUniMedida) {
		this.idUniMedida = idUniMedida;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Casilla> getCasillas() {
		return this.casillas;
	}

	public void setCasillas(List<Casilla> casillas) {
		this.casillas = casillas;
	}

	public Casilla addCasilla(Casilla casilla) {
		getCasillas().add(casilla);
		casilla.setUnidMedida(this);

		return casilla;
	}

	public Casilla removeCasilla(Casilla casilla) {
		getCasillas().remove(casilla);
		casilla.setUnidMedida(null);

		return casilla;
	}

}