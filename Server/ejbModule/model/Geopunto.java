package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the GEOPUNTOS database table.
 * 
 */
@Entity
@Table(name="GEOPUNTOS")
@NamedQuery(name="Geopunto.findAll", query="SELECT g FROM Geopunto g")
public class Geopunto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="GEOPUNTOS_IDGEOPUNTO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GEOPUNTOS_IDGEOPUNTO_GENERATOR")
	@Column(name="ID_GEOPUNTO")
	private long idGeopunto;

	private BigDecimal latitud;

	private BigDecimal longitud;

	//bi-directional many-to-one association to ActiCampo
	@OneToMany(mappedBy="geopunto")
	private List<ActiCampo> actiCampos;

	public Geopunto() {
	}

	public long getIdGeopunto() {
		return this.idGeopunto;
	}

	public void setIdGeopunto(long idGeopunto) {
		this.idGeopunto = idGeopunto;
	}

	public BigDecimal getLatitud() {
		return this.latitud;
	}

	public void setLatitud(BigDecimal latitud) {
		this.latitud = latitud;
	}

	public BigDecimal getLongitud() {
		return this.longitud;
	}

	public void setLongitud(BigDecimal longitud) {
		this.longitud = longitud;
	}

	public List<ActiCampo> getActiCampos() {
		return this.actiCampos;
	}

	public void setActiCampos(List<ActiCampo> actiCampos) {
		this.actiCampos = actiCampos;
	}

	public ActiCampo addActiCampo(ActiCampo actiCampo) {
		getActiCampos().add(actiCampo);
		actiCampo.setGeopunto(this);

		return actiCampo;
	}

	public ActiCampo removeActiCampo(ActiCampo actiCampo) {
		getActiCampos().remove(actiCampo);
		actiCampo.setGeopunto(null);

		return actiCampo;
	}

}