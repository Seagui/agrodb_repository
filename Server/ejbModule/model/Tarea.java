package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the TAREAS database table.
 * 
 */
@Entity
@Table(name="TAREAS")
@NamedQuery(name="Tarea.findAll", query="SELECT t FROM Tarea t")
public class Tarea implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TAREAS_IDTAREA_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TAREAS_IDTAREA_GENERATOR")
	@Column(name="ID_TAREA")
	private long idTarea;

	private String nombre;

	//bi-directional many-to-one association to RolesTarea
	@OneToMany(mappedBy="tarea")
	private List<RolesTarea> rolesTareas;

	public Tarea() {
	}

	public long getIdTarea() {
		return this.idTarea;
	}

	public void setIdTarea(long idTarea) {
		this.idTarea = idTarea;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<RolesTarea> getRolesTareas() {
		return this.rolesTareas;
	}

	public void setRolesTareas(List<RolesTarea> rolesTareas) {
		this.rolesTareas = rolesTareas;
	}

	public RolesTarea addRolesTarea(RolesTarea rolesTarea) {
		getRolesTareas().add(rolesTarea);
		rolesTarea.setTarea(this);

		return rolesTarea;
	}

	public RolesTarea removeRolesTarea(RolesTarea rolesTarea) {
		getRolesTareas().remove(rolesTarea);
		rolesTarea.setTarea(null);

		return rolesTarea;
	}

}