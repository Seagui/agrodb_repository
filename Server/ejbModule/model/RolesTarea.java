package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ROLES_TAREAS database table.
 * 
 */
@Entity
@Table(name="ROLES_TAREAS")
@NamedQuery(name="RolesTarea.findAll", query="SELECT r FROM RolesTarea r")
public class RolesTarea implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ROLES_TAREAS_IDROLTAREA_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ROLES_TAREAS_IDROLTAREA_GENERATOR")
	@Column(name="ID_ROL_TAREA")
	private long idRolTarea;

	//bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name="ID_ROL")
	private Role role;

	//bi-directional many-to-one association to Tarea
	@ManyToOne
	@JoinColumn(name="ID_TAREA")
	private Tarea tarea;

	public RolesTarea() {
	}

	public long getIdRolTarea() {
		return this.idRolTarea;
	}

	public void setIdRolTarea(long idRolTarea) {
		this.idRolTarea = idRolTarea;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Tarea getTarea() {
		return this.tarea;
	}

	public void setTarea(Tarea tarea) {
		this.tarea = tarea;
	}

}