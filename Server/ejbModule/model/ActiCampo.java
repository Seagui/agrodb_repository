package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ACTI_CAMPO database table.
 * 
 */
@Entity
@Table(name="ACTI_CAMPO")
@NamedQuery(name="ActiCampo.findAll", query="SELECT a FROM ActiCampo a")
public class ActiCampo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACTI_CAMPO_IDACTICAMPO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACTI_CAMPO_IDACTICAMPO_GENERATOR")
	@Column(name="ID_ACTI_CAMPO")
	private long idActiCampo;

	private String activo;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	//bi-directional many-to-one association to EstaMuestreo
	@ManyToOne
	@JoinColumn(name="ID_EST_MUESTREO")
	private EstaMuestreo estaMuestreo;

	//bi-directional many-to-one association to Formulario
	@ManyToOne
	@JoinColumn(name="ID_FORMULARIO")
	private Formulario formulario;

	//bi-directional many-to-one association to Geopunto
	@ManyToOne
	@JoinColumn(name="ID_GEOPUNTO")
	private Geopunto geopunto;

	//bi-directional many-to-one association to MetoMuestreo
	@ManyToOne
	@JoinColumn(name="ID_MET_MUESTREO")
	private MetoMuestreo metoMuestreo;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="ID_USUARIO")
	private Usuario usuario;

	//bi-directional many-to-one association to Zona
	@ManyToOne
	@JoinColumn(name="ID_ZONA")
	private Zona zona;

	//bi-directional many-to-one association to Dato
	@OneToMany(mappedBy="actiCampo")
	private List<Dato> datos;

	public ActiCampo() {
	}

	public long getIdActiCampo() {
		return this.idActiCampo;
	}

	public void setIdActiCampo(long idActiCampo) {
		this.idActiCampo = idActiCampo;
	}

	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public EstaMuestreo getEstaMuestreo() {
		return this.estaMuestreo;
	}

	public void setEstaMuestreo(EstaMuestreo estaMuestreo) {
		this.estaMuestreo = estaMuestreo;
	}

	public Formulario getFormulario() {
		return this.formulario;
	}

	public void setFormulario(Formulario formulario) {
		this.formulario = formulario;
	}

	public Geopunto getGeopunto() {
		return this.geopunto;
	}

	public void setGeopunto(Geopunto geopunto) {
		this.geopunto = geopunto;
	}

	public MetoMuestreo getMetoMuestreo() {
		return this.metoMuestreo;
	}

	public void setMetoMuestreo(MetoMuestreo metoMuestreo) {
		this.metoMuestreo = metoMuestreo;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Zona getZona() {
		return this.zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	public List<Dato> getDatos() {
		return this.datos;
	}

	public void setDatos(List<Dato> datos) {
		this.datos = datos;
	}

	public Dato addDato(Dato dato) {
		getDatos().add(dato);
		dato.setActiCampo(this);

		return dato;
	}

	public Dato removeDato(Dato dato) {
		getDatos().remove(dato);
		dato.setActiCampo(null);

		return dato;
	}

}