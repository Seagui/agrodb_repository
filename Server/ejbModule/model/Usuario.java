package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the USUARIOS database table.
 * 
 */
@Entity
@Table(name="USUARIOS")
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="USUARIOS_IDUSUARIO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USUARIOS_IDUSUARIO_GENERATOR")
	@Column(name="ID_USUARIO")
	private long idUsuario;

	private String activo;

	private String apellido;

	private BigDecimal cedula;

	private String contrasenia;

	private String email;

	private String instituto;

	@Column(name="NOMB_USUARIO")
	private String nombUsuario;

	private String nombre;

	private String profesion;

	//bi-directional many-to-one association to ActiCampo
	@OneToMany(mappedBy="usuario")
	private List<ActiCampo> actiCampos;

	//bi-directional many-to-one association to Formulario
	@OneToMany(mappedBy="usuario")
	private List<Formulario> formularios;

	//bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name="ID_ROL")
	private Role role;

	public Usuario() {
	}

	public long getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public BigDecimal getCedula() {
		return this.cedula;
	}

	public void setCedula(BigDecimal cedula) {
		this.cedula = cedula;
	}

	public String getContrasenia() {
		return this.contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInstituto() {
		return this.instituto;
	}

	public void setInstituto(String instituto) {
		this.instituto = instituto;
	}

	public String getNombUsuario() {
		return this.nombUsuario;
	}

	public void setNombUsuario(String nombUsuario) {
		this.nombUsuario = nombUsuario;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getProfesion() {
		return this.profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public List<ActiCampo> getActiCampos() {
		return this.actiCampos;
	}

	public void setActiCampos(List<ActiCampo> actiCampos) {
		this.actiCampos = actiCampos;
	}

	public ActiCampo addActiCampo(ActiCampo actiCampo) {
		getActiCampos().add(actiCampo);
		actiCampo.setUsuario(this);

		return actiCampo;
	}

	public ActiCampo removeActiCampo(ActiCampo actiCampo) {
		getActiCampos().remove(actiCampo);
		actiCampo.setUsuario(null);

		return actiCampo;
	}

	public List<Formulario> getFormularios() {
		return this.formularios;
	}

	public void setFormularios(List<Formulario> formularios) {
		this.formularios = formularios;
	}

	public Formulario addFormulario(Formulario formulario) {
		getFormularios().add(formulario);
		formulario.setUsuario(this);

		return formulario;
	}

	public Formulario removeFormulario(Formulario formulario) {
		getFormularios().remove(formulario);
		formulario.setUsuario(null);

		return formulario;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}