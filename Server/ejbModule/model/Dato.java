package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the DATOS database table.
 * 
 */
@Entity
@Table(name="DATOS")
@NamedQuery(name="Dato.findAll", query="SELECT d FROM Dato d")
public class Dato implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DATOS_IDDATO_GENERATOR" )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DATOS_IDDATO_GENERATOR")
	@Column(name="ID_DATO")
	private long idDato;

	private String valor;

	//bi-directional many-to-one association to ActiCampo
	@ManyToOne
	@JoinColumn(name="ID_ACTI_CAMPO")
	private ActiCampo actiCampo;

	//bi-directional many-to-one association to Casilla
	@ManyToOne
	@JoinColumn(name="ID_CASILLA")
	private Casilla casilla;

	public Dato() {
	}

	public long getIdDato() {
		return this.idDato;
	}

	public void setIdDato(long idDato) {
		this.idDato = idDato;
	}

	public String getValor() {
		return this.valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public ActiCampo getActiCampo() {
		return this.actiCampo;
	}

	public void setActiCampo(ActiCampo actiCampo) {
		this.actiCampo = actiCampo;
	}

	public Casilla getCasilla() {
		return this.casilla;
	}

	public void setCasilla(Casilla casilla) {
		this.casilla = casilla;
	}

}