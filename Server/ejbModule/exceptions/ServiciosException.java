package exceptions;

//Clase hija de Excception para manejar excepciones propias ante problemas en el Servidor por ejemplo, de integridad.
public class ServiciosException extends Exception {

	private static final long serialVersionUID = 1L;

	// Constructor
	public ServiciosException (String mensaje){
	
		// Se pasa el mensaje de la excepcion a la clase padre Exception
		super(mensaje);
	
	} 
	
}