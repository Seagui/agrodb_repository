package services;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import daos.DAORolesLocal;
import daos.DAORolesTareasLocal;
import daos.DAOTareasLocal;
import daos.DAOUsuariosLocal;
import exceptions.ServiciosException;
import model.Role;
import model.RolesTarea;
import model.Tarea;
import model.Usuario;

@Stateless
public class UsuariosBean implements UsuariosBeanRemote {

	@EJB
	DAOUsuariosLocal usersDAO;
	@EJB
	DAORolesLocal rolesDAO;
	@EJB
	DAOTareasLocal tasksDAO;
	@EJB
	DAORolesTareasLocal rolesTasksDAO;
	
    public UsuariosBean() { }

    @Override
    public Usuario Login(String nombreUsuario, String password) {
		return usersDAO.Login(nombreUsuario, password);
    }
    
    @Override
    public int GetCountAdmins(Role role) {
    	return usersDAO.GetCountAdmins(role);
    }
    
	@Override
	public List<Usuario> SelectUsers() {
		return usersDAO.SelectUsers();
	}
	
	@Override
	public List<Usuario> SelectUsers(String nombreUsuario, String nombre, String apellido, Role role) {
		return usersDAO.SelectUsers(nombreUsuario, nombre, apellido, role);
	}

	@Override
	public boolean CreateUser(String nombre, String apellido, String email, String nombreUsuario, String password, String nombreRol, String cedula, String profesion, String instituto) throws ServiciosException {
		
		Usuario newUser = new Usuario();
		newUser.setNombre(nombre);
		newUser.setApellido(apellido);
		newUser.setEmail(email);
		newUser.setNombUsuario(nombreUsuario);
		newUser.setContrasenia(password);
		newUser.setActivo("true");
				
		Role role = new Role();	
		role.setNombre(nombreRol);
		role.setIdRol(rolesDAO.GetIDRole(nombreRol));
		newUser.setRole(role);

		if (nombreRol.equals("Experto") || nombreRol.equals("Administrador")) {
						
			if (nombreRol.equals("Experto")) {
							
				newUser.setCedula(new BigDecimal(cedula));
				newUser.setProfesion(profesion);
						
			} 
			else {
							
				newUser.setCedula(new BigDecimal(cedula));
				newUser.setInstituto(instituto);
							
			}	
			
		}
		
		usersDAO.CreateUser(newUser);
		
		return true;
	}

	@Override
	public boolean UpdateUser(String nombre, String apellido, String email, String nombreUsuario, String password, String nombreRol, String cedula, String profesion, String instituto, String oldUser) throws ServiciosException {
		
		Usuario newUser = new Usuario();
		newUser.setNombre(nombre);
		newUser.setApellido(apellido);
		newUser.setEmail(email);
		newUser.setNombUsuario(nombreUsuario);
		newUser.setContrasenia(password);
		newUser.setIdUsuario(usersDAO.GetIDUser(nombreUsuario));
		newUser.setActivo("true");
		
		Role role = new Role();			
		role.setNombre(nombreRol);
		role.setIdRol(rolesDAO.GetIDRole(nombreRol));
		newUser.setRole(role);

		if (nombreRol.equals("Experto") || nombreRol.equals("Administrador")) {
						
			if (nombreRol.equals("Experto")) {
							
				newUser.setCedula(new BigDecimal(cedula));
				newUser.setProfesion(profesion);
						
			} 
			else {
							
				newUser.setCedula(new BigDecimal(cedula));
				newUser.setInstituto(instituto);
							
			}	
			
		}
		
		usersDAO.UpdateUser(newUser, oldUser);
		
		return true;
	}
	
	@Override
	public boolean UpdateUserPassword(Usuario usuario, String password) throws ServiciosException {
		
		usuario.setContrasenia(password);
			
		usersDAO.UpdateUser(usuario, usuario.getNombUsuario());
		
		return true;
	}

	@Override
	public boolean DeleteUser(Long id) throws ServiciosException {
		
		usersDAO.DeleteUser(id);
		
		return true;
	}

	@Override
	public List<Role> SelectRoles() {
		return rolesDAO.SelectRoles();
	}

	@Override
	public boolean CreateRole(String nombre) throws ServiciosException {
		
		Role newRole = new Role();
		newRole.setNombre(nombre);
											
		rolesDAO.CreateRole(newRole);
		
		return true;
	}

	@Override
	public boolean UpdateRole(String nombre, Long id) throws ServiciosException {
		
		Role role = new Role();
		role.setNombre(nombre);
		role.setIdRol(id);
		
		rolesDAO.UpdateRole(role);
		
		return true;
	}

	@Override
	public boolean DeleteRole(Long id) throws ServiciosException {

		rolesDAO.DeleteRole(id);
		
		return true;
	}
	
	@Override
	public List<Tarea> SelectTasks() {
		return tasksDAO.SelectTasks();
	}
	
	@Override
	public List<Tarea> SelectTasks(String roleName) {
		return tasksDAO.SelectTasks(roleName);
	}

	@Override
	public boolean CreateTask(String nombre) throws ServiciosException {

		Tarea newTask = new Tarea();
		newTask.setNombre(nombre);
		
		tasksDAO.CreateTask(newTask);
		
		return true;
	}

	@Override
	public boolean UpdateTask(String nombre, Long id) throws ServiciosException {
		
		Tarea task = new Tarea();
		task.setNombre(nombre);
		task.setIdTarea(id);
		
		tasksDAO.UpdateTask(task);
		
		return true;
	}

	@Override
	public boolean DeleteTask(Long id) throws ServiciosException {
		
		tasksDAO.DeleteTask(id);
		
		return true;
	}
	
	@Override
	public int GetCountTasks(Tarea task) {
		return rolesTasksDAO.GetCountTasks(task);
	}
	
	@Override
	public boolean CreateRoleTask(Tarea task, Role role) throws ServiciosException {
						
		RolesTarea newRoleTask = new RolesTarea();
		newRoleTask.setRole(role);
		newRoleTask.setTarea(task);
		rolesTasksDAO.CreateRoleTask(newRoleTask);
		
		return true;
	}
	
	@Override
	public boolean CreateRoleTask(String nombre, Role role) throws ServiciosException {
						
		Tarea newTask = new Tarea();
		newTask.setNombre(nombre);
		
		tasksDAO.CreateTask(newTask);
		
		newTask.setIdTarea(tasksDAO.GetIDTask(nombre));
				
		RolesTarea newRoleTask = new RolesTarea();
		newRoleTask.setRole(role);
		newRoleTask.setTarea(newTask);
		rolesTasksDAO.CreateRoleTask(newRoleTask);
		
		return true;
	}


	@Override
	public boolean DeleteRoleTask(Tarea task, Role role) throws ServiciosException {

		rolesTasksDAO.DeleteRoleTask(rolesTasksDAO.GetIDRoleTask(task, role));
		
		return true;
	}

}