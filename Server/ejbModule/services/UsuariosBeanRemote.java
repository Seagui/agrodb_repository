package services;

import java.util.List;

import javax.ejb.Remote;

import exceptions.ServiciosException;
import model.Role;
import model.Tarea;
import model.Usuario;

@Remote
public interface UsuariosBeanRemote {

	public Usuario Login(String nombreUsuario, String password);																																									//Metodo para obtener si un usuario esta logeado
	public int GetCountAdmins(Role role);																																															//Metodo para obtener la cantidad de administradores en el sistema
	public List<Usuario> SelectUsers();																																																//Metodo para obtener todos los usuarios
	public List<Usuario> SelectUsers(String nombreUsuario, String nombre, String apellido, Role role);																																//Metodo para listar usuarios filtrados
	public boolean CreateUser(String nombre, String apellido, String email, String nombreUsuario, String password, String nombreRol, String cedula, String profesion, String instituto) throws ServiciosException;					//Metodo para crear un usuario
	public boolean UpdateUser(String nombre, String apellido, String email, String nombreUsuario, String password, String nombreRol, String cedula, String profesion, String instituto, String oldUser) throws ServiciosException;	//Metodo para modificar un usuario
	public boolean UpdateUserPassword(Usuario usuario, String password) throws ServiciosException;																																	//Metodo para modificar la contraseņa de un usuario
	public boolean DeleteUser(Long id) throws ServiciosException;																																									//Metodo para eliminar un usuario
	public List<Role> SelectRoles();																																																//Metodo para obtener el listado de roles
	public boolean CreateRole(String nombre) throws ServiciosException;																																								//Metodo para crear un rol
	public boolean UpdateRole(String nombre, Long id) throws ServiciosException;																																					//Metodo para modificar un rol
	public boolean DeleteRole(Long id) throws ServiciosException;																																									//Metodo para eliminar un rol
	public List<Tarea> SelectTasks();																																																//Metodo para seleccionar todas las tareas
	public List<Tarea> SelectTasks(String roleName);																																												//Metodo para seleccionar todas las tareas de un rol
	public boolean CreateTask(String nombre) throws ServiciosException;																																								//Metodo para crear una tarea
	public boolean UpdateTask(String nombre, Long id) throws ServiciosException;																																					//Metodo para modificar una tarea
	public boolean DeleteTask(Long id) throws ServiciosException;																																									//Metodo para eliminar una tarea
	public int GetCountTasks(Tarea task);																																															//Metodo para obtener la cantidad de relaciones en la que esta una tarea
	public boolean CreateRoleTask(Tarea task, Role role) throws ServiciosException;																																					//Metodo para crear una relacion tarea/rol
	public boolean CreateRoleTask(String nombre, Role role) throws ServiciosException;																																				//Metodo para crear una tarea y relacionarla a un rol
	public boolean DeleteRoleTask(Tarea task, Role role) throws ServiciosException;																																					//Metodo para eliminar una relacion tarea/rol
	
}