package services;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import daos.DAOActiCampoLocal;
import daos.DAODatosLocal;
import daos.DAOEstaMuestreoLocal;
import daos.DAOMetoMuestreoLocal;
import daos.DAOTareasLocal;
import daos.DAOZonaLocal;
import exceptions.ServiciosException;
import model.ActiCampo;
import model.Casilla;
import model.Dato;
import model.EstaMuestreo;
import model.Formulario;
import model.MetoMuestreo;
import model.Role;
import model.Tarea;
import model.Usuario;
import model.Zona;

@Stateless
public class ActiCampoBean implements ActiCampoBeanRemote {

	@EJB
	DAOActiCampoLocal activitysDAO;
	@EJB
	DAOMetoMuestreoLocal methodsDAO;
	@EJB
	DAOEstaMuestreoLocal stationsDAO;
	@EJB
	DAOZonaLocal zonesDAO;
	@EJB
	DAODatosLocal dataDAO;
	@EJB
	DAOTareasLocal tasksDAO;
	
    public ActiCampoBean() { }

	@Override
	public List<ActiCampo> SelectActivitys() {
		return activitysDAO.SelectActivitys();
	}
	
	@Override
	public List<ActiCampo> SelectActivitys(String form, Usuario user) {
		
		boolean canSeeAll = false;
		for (Tarea task : tasksDAO.SelectTasks(user.getRole().getNombre())) {
			if (task.getNombre().equals("Gestion de Formularios")) {
				canSeeAll = true;
			}
		}
		
		
		if (canSeeAll) {
			return activitysDAO.SelectActivitys(form);
		}
		else {
			return activitysDAO.SelectActivitys(form, user);
		}
	}
	
	@Override
	public List<ActiCampo> SelectActivitys(MetoMuestreo method, Role rol, Date fechaInicio, Date fechaFin) {
		return activitysDAO.SelectActivitys(method, rol, fechaInicio, fechaFin);
	}

	@Override
	public boolean CreateActivity(Date fecha, Usuario usuario, Formulario formulario, MetoMuestreo metodoMuestreo, EstaMuestreo estacionMuestreo, Zona zona) throws ServiciosException {
		
		ActiCampo newActivity = new ActiCampo();
		newActivity.setFecha(fecha);
		newActivity.setUsuario(usuario);
		newActivity.setFormulario(formulario);
		newActivity.setMetoMuestreo(metodoMuestreo);
		newActivity.setEstaMuestreo(estacionMuestreo);
		newActivity.setZona(zona);
		
		activitysDAO.CreateActivity(newActivity);
		
		return true;
	}

	@Override
	public List<MetoMuestreo> SelectMethods() {
		return methodsDAO.SelectMethods();
	}

	@Override
	public boolean CreateMethods(String method) throws ServiciosException {

		MetoMuestreo newMethod = new MetoMuestreo();
		newMethod.setMetodo(method);
		
		methodsDAO.CreateMethod(newMethod);
		
		return true;
	}

	@Override
	public List<EstaMuestreo> SelectStations() {
		return stationsDAO.SelectStation();
	}

	@Override
	public boolean CreateStation(String station) throws ServiciosException {
		
		EstaMuestreo newStation = new EstaMuestreo();
		newStation.setEstacion(station);
		
		stationsDAO.CreateStation(newStation);
		
		return true;
	}

	@Override
	public List<Zona> SelectZones() {
		return zonesDAO.SelectZones();
	}

	@Override
	public boolean CreateZone(String zone) throws ServiciosException {

		Zona newZone = new Zona();
		newZone.setZona(zone);
		
		zonesDAO.CreateZone(newZone);
		
		return true;
	}

	@Override
	public boolean CreateData(ActiCampo activity, Casilla box, String valor) throws ServiciosException {
		
		Dato newData = new Dato();
		newData.setActiCampo(activity);
		newData.setCasilla(box);
		newData.setValor(valor);
		
		dataDAO.CreateData(newData);
		
		return true;
	}

	@Override
	public ActiCampo SelectActivity(Date fecha, Usuario usuario, Formulario formulario) {
		return activitysDAO.SelectActivity(fecha, usuario, formulario);
	}

	@Override
	public List<Dato> SelectData(ActiCampo activity) {
		return dataDAO.SelectData(activity);
	}
    
}