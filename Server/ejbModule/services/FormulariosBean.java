package services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import daos.DAOFormCasiLocal;
import daos.DAOFormulariosLocal;
import exceptions.ServiciosException;
import model.Casilla;
import model.FormCasi;
import model.Formulario;
import model.Usuario;

@Stateless
public class FormulariosBean implements FormulariosBeanRemote {

	@EJB
	DAOFormulariosLocal formsDAO;
	@EJB
	DAOFormCasiLocal formBoxDAO;
	
    public FormulariosBean() { }

	@Override
	public List<Formulario> SelectForms() {
		return formsDAO.SelectForms();
	}

	@Override
	public boolean CreateForm(String nombre, String resumen, Usuario usuario) throws ServiciosException {
		  
  	  	Formulario newForm = new Formulario();
  	  	newForm.setNombFormulario(nombre);
  	  	newForm.setResumen(resumen);
  	  	newForm.setUsuario(usuario);

		formsDAO.CreateForm(newForm);
		
		return true;
	}

	@Override
	public boolean UpdateForm(String nombre, String resumen, Usuario usuario, Long id, String nombreFormulario) throws ServiciosException {
		
  	  	Formulario newForm = new Formulario();
  	  	newForm.setNombFormulario(nombre);
  	  	newForm.setResumen(resumen);
  	  	newForm.setUsuario(usuario);
  	  	newForm.setIdFormulario(id);

  	  	formsDAO.UpdateForm(newForm, nombreFormulario);
		
  	  	return true;
	}

	@Override
	public boolean AddBox(Casilla box, Formulario form) throws ServiciosException {

		FormCasi newFormCasi = new FormCasi();
		newFormCasi.setCasilla(box);
		newFormCasi.setFormulario(form);
		
		formBoxDAO.CreateFormBox(newFormCasi);
		
		return true;
	}

	@Override
	public boolean RemoveBox(Casilla box, Formulario form) throws ServiciosException {
		
		formBoxDAO.DeleteFormBox(formBoxDAO.GetIDRoleTask(form, box));
		
		return true;
	}

}