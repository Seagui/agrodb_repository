package services;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import daos.DAOCasillasLocal;
import daos.DAOTipoInputLocal;
import daos.DAOUniMedidaLocal;
import exceptions.ServiciosException;
import model.Casilla;
import model.TipoInput;
import model.UnidMedida;

@Stateless
public class CasillasBean implements CasillasBeanRemote {

	@EJB
	DAOCasillasLocal boxsDAO;
	@EJB
	DAOUniMedidaLocal unitsDAO;
	@EJB
	DAOTipoInputLocal inputsDAO;
	
    public CasillasBean() { }

	@Override
    public List<Casilla> SelectBoxs() {
    	return boxsDAO.SelectBoxs();
    }
    
	@Override
    public List<Casilla> SelectBoxs(String nombreFormulario) {
    	return boxsDAO.SelectBoxs(nombreFormulario);
    }
    
	@Override
	public boolean CreateBox(String parametro, String descripcion, TipoInput input, UnidMedida unidad) throws ServiciosException {

		Casilla newBox = new Casilla();
		newBox.setParametro(parametro);
		newBox.setDescripcion(descripcion);
		newBox.setTipoInput(input);
		newBox.setUnidMedida(unidad);
		
		boxsDAO.CreateBox(newBox);
		
		return true;
	}

	@Override
	public List<UnidMedida> SelectUnits() {
		return unitsDAO.SelectUnits();
	}

	@Override
	public boolean CreateUnit(String unit) throws ServiciosException {
		
		UnidMedida newUnit = new UnidMedida();
		newUnit.setNombre(unit);
											
		unitsDAO.CreateUnit(newUnit);
		
		return true;
	}

	@Override
	public List<TipoInput> SelectInputs() {
		return inputsDAO.SelectInputs();
	}
    
}