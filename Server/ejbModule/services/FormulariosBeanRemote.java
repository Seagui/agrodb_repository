package services;

import java.util.List;

import javax.ejb.Remote;

import exceptions.ServiciosException;
import model.Casilla;
import model.Formulario;
import model.Usuario;

@Remote
public interface FormulariosBeanRemote {

	public List<Formulario> SelectForms();																									//Metodo para seleccionar todos los formularios
	public boolean CreateForm(String nombre, String resumen, Usuario usuario) throws ServiciosException;									//Metodo para crear un formulario
	public boolean UpdateForm(String nombre, String resumen, Usuario usuario, Long id, String nombreFormulario) throws ServiciosException;	//Metodo para modificar un formulario
	public boolean AddBox(Casilla box, Formulario form) throws ServiciosException;															//Metodo para a�adir una casilla a un formulario determinado
	public boolean RemoveBox(Casilla box, Formulario form) throws ServiciosException;														//Metodo para quitar una casilla de un formulario determinado
	
}