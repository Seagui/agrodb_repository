package services;

import java.util.List;

import javax.ejb.Remote;

import exceptions.ServiciosException;
import model.Casilla;
import model.TipoInput;
import model.UnidMedida;

@Remote
public interface CasillasBeanRemote {

	public List<Casilla> SelectBoxs();																								//Metodo para obtener todas las casillas
	public List<Casilla> SelectBoxs(String nombreFormulario);																		//Metodo para obtener las casillas de un formulario
	public boolean CreateBox(String parametro, String descripcion, TipoInput input, UnidMedida unidad) throws ServiciosException;	//Metodo para crear una casilla
	public List<UnidMedida> SelectUnits();																							//Metodo para obtener todas las unidades
	public boolean CreateUnit(String unit) throws ServiciosException;																//Metodo para crear una unidad
	public List<TipoInput> SelectInputs();																							//Metodo para obtener todos los tipos de input
	
}