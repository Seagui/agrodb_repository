package services;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import exceptions.ServiciosException;
import model.ActiCampo;
import model.Casilla;
import model.Dato;
import model.EstaMuestreo;
import model.Formulario;
import model.MetoMuestreo;
import model.Role;
import model.Usuario;
import model.Zona;

@Remote
public interface ActiCampoBeanRemote {

	public List<ActiCampo> SelectActivitys();																																			//Metodo para obtener todas las actividades
	public List<ActiCampo> SelectActivitys(String form, Usuario user);																													//Metodo para listar los muestreos de un formulario por determinado usuario
	public List<ActiCampo> SelectActivitys(MetoMuestreo method, Role rol, Date fechaInicio, Date fechaFin);																				//Metodo para realizar un reporte
	public boolean CreateActivity(Date fecha, Usuario usuario, Formulario formulario, MetoMuestreo metodoMuestreo, EstaMuestreo estacionMuestreo, Zona zona) throws ServiciosException;	//Metodo para crear una Actividad
	public List<MetoMuestreo> SelectMethods();																																			//Metodo para obtener todos metodos
	public boolean CreateMethods(String method) throws ServiciosException;																												//Metodo para crear un metodo
	public List<EstaMuestreo> SelectStations();																																			//Metodo para obtener todas las estacioes
	public boolean CreateStation(String station) throws ServiciosException;																												//Metodo para crear una estacion
	public List<Zona> SelectZones();																																					//Metodo para selecionar todas las zonas
	public boolean CreateZone(String zone) throws ServiciosException;																													//Metodo para crear una zona
	public boolean CreateData(ActiCampo activity, Casilla box, String valor) throws ServiciosException;																					//Metodo para crear el registro de una casilla
	public ActiCampo SelectActivity(Date fecha, Usuario usuario, Formulario formulario);																								//Metodo para obtener una actividad
	public List<Dato> SelectData(ActiCampo activity);																																	//Metodo para obtener todos los datos de una actividad
	
}