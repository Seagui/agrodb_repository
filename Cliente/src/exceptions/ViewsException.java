package exceptions;

// Clase hija de Excception para manejar excepciones propias ante problemas en el Cliente como verificaciones.
public class ViewsException extends Exception {

	private static final long serialVersionUID = 1L;

	// Constructor
	public ViewsException (String mensaje) {
	
		// Se pasa el mensaje de la excepcion a la clase padre Exception
		super(mensaje);
	
	} 
	
}