package utilitis;

import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JTable;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class XLS {

	public static void GenerateXLS(String name, JTable table) {
		
		//Se crea un libro
		@SuppressWarnings("resource")
		Workbook libro = new HSSFWorkbook();
		
		//Se crea la hoja
		Sheet hoja = libro.createSheet(name);
		
		//Se crean las filas
		for (int row = 0; row < table.getRowCount(); row++) {
			
			Row fila = hoja.createRow(row);
			
			//Se crean las celdas
			for (int column = 0; column < table.getColumnCount(); column++) {
				
				Cell celda = fila.createCell(column);
				
				//Se a�ade el valor en la celda
				celda.setCellValue(table.getValueAt(row, column).toString());
				
			}
			
		}
					
		//Se busca la direccion
		JFileChooser chooser = new JFileChooser(); 
		chooser.setCurrentDirectory(new java.io.File(".")); 
		chooser.setDialogTitle("select folder"); 
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 
		chooser.setAcceptAllFileFilterUsed(false); 
		
	    int userSelection = chooser.showSaveDialog(null);
	    if (userSelection == JFileChooser.APPROVE_OPTION) {
	    
			//Se crea el archivo
			String file = chooser.getCurrentDirectory().getAbsolutePath() + File.separator + name + ".xls";
			
			//Se guarda el archivo
			FileOutputStream out;
			try {
				out = new FileOutputStream(file);
				libro.write(out);
			} catch (IOException e) {
				e.printStackTrace();
			}
	    	
	    }
		
	}
	
}