package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import exceptions.ServiciosException;
import exceptions.ViewsException;
import model.Role;
import services.UsuariosBeanRemote;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.Color;
import java.awt.Font;

public class UsersDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldNombre;
	private JTextField textFieldApellido;
	private JTextField textFieldEmail;
	private JTextField textFieldUsuario;
	private JTextField textFieldCedula;
	private JTextField textFieldProfesion;
	private JTextField textFieldInstituto;
	JComboBox<String> comboBoxRol;
	JLabel lblTitle;
	JButton btnCrear;

	UsuariosBeanRemote usersBean = (UsuariosBeanRemote) InitialContext.doLookup("AgroDB_Server/UsuariosBean!services.UsuariosBeanRemote");
	
	List<Role> roles;
	boolean update = false;
	String oldUser;
	private JPasswordField textFieldContrasenia;
	
	public void SetUpdate(String[] data) {
		
		//Se coloca los labels para modificar
		lblTitle.setText("Formulario de moficación");
		btnCrear.setText("Modificar");
		
		//Se setea el usuario a modificar
		update = true;
		oldUser = data[3];
		
		//Se setean los datos en los campos
		textFieldNombre.setText(data[0]);
		textFieldApellido.setText(data[1]);
		textFieldEmail.setText(data[2]);
		textFieldUsuario.setText(data[3]);
		textFieldContrasenia.setText(data[4]);
		textFieldCedula.setText(data[5]);
		textFieldProfesion.setText(data[6]);
		textFieldInstituto.setText(data[7]);
		comboBoxRol.setSelectedItem(data[8]);;
		
		//Se desabilitan los campos de usuario y contraseña
		textFieldUsuario.setEnabled(false);
		textFieldContrasenia.setEnabled(false);
		
	}
	
	//Se crea un Usuario
	@SuppressWarnings("deprecation")
	private void Action() throws ServiciosException, ViewsException {
		
		//Se verifica que en el combo no este seleccionado el mensaje informativo
		if (comboBoxRol.getSelectedIndex() <= 0)
			throw new ViewsException("Debes de seleccionar un Rol");
		
		//Si los campos no estan nulos, se interpreta se quiere ingresar algo
		if (textFieldNombre.getText() != null && textFieldApellido.getText() != null && textFieldUsuario.getText() != null && textFieldContrasenia.getText() != null) {
						
			//Se verifica los campos no esten vacios
			if (textFieldNombre.getText().isEmpty() || textFieldApellido.getText().isEmpty() || textFieldUsuario.getText().isEmpty() || textFieldContrasenia.getText().isEmpty())
				throw new ViewsException("No puedes dejar campos obligatorios vacios");
					
			if (!textFieldEmail.getText().isEmpty())
				if (!VerifyMail(textFieldEmail.getText()))
					throw new ViewsException("El correo ingresado no es valido");
			
			if (!Home.VerifyPassword(textFieldContrasenia.getText()))
				throw new ViewsException("La contrasenia ingresada no es vailda");
			
			if (comboBoxRol.getSelectedItem().equals("Experto") || comboBoxRol.getSelectedItem().equals("Administrador")) {
				
				if (!VerifyCedula(textFieldCedula.getText()))
					throw new ViewsException("La cedula ingresada no es valida, solo puede contener numeros");
				
				if (textFieldCedula.getText() == null)
					throw new ViewsException("No puedes dejar la Cedula vacia");
					
				if(textFieldCedula.getText().isEmpty())
					throw new ViewsException("No puedes dejar la Cedula vacia");
				
			}
		
			if (update) {
				
				//Se modifica el usuario
				if (usersBean.UpdateUser(textFieldNombre.getText(), textFieldApellido.getText(), textFieldEmail.getText(), textFieldUsuario.getText(), textFieldContrasenia.getText(),
						comboBoxRol.getSelectedItem().toString(), textFieldCedula.getText(), textFieldProfesion.getText(), textFieldInstituto.getText(), oldUser)) {
					
					JOptionPane.showMessageDialog(null, "El Usuario ha sido modificado correctamente");
					
					dispose();
					
				}
								
			} else {
				
				//Se crea el usuario
				if (usersBean.CreateUser(textFieldNombre.getText(), textFieldApellido.getText(), textFieldEmail.getText(), textFieldUsuario.getText(), textFieldContrasenia.getText(),
						comboBoxRol.getSelectedItem().toString(), textFieldCedula.getText(), textFieldProfesion.getText(), textFieldInstituto.getText())) {
					
					JOptionPane.showMessageDialog(null, "El Usuario ha sido creado correctamente");

					CleanFields();
					
				}
			
			}
						
		}
		
	}
	
	//Se verifica el mail tenga un patron correcto
	private boolean VerifyMail(String mail) {
		//Se crea el patron
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        
        //Se realiza el filtrado
        Matcher mather = pattern.matcher(mail);
        return mather.find();
	}
	
	//Se verifica que la cedula tenga solo numeros
	private boolean VerifyCedula(String cedula) {
		try {
			@SuppressWarnings("unused")
			BigDecimal bd = new BigDecimal(cedula);
			return true;
		} catch (Exception e) { 
			return false;
		}
	}
	
	//Metodo para limpiar los componentes
	private void CleanFields() {
		
		//Se vacian los campos
		textFieldNombre.setText("");
		textFieldApellido.setText("");
		textFieldEmail.setText("");
		textFieldUsuario.setText("");
		textFieldCedula.setText("");
		textFieldProfesion.setText("");
		textFieldInstituto.setText("");
		textFieldContrasenia.setText("");
		comboBoxRol.setSelectedIndex(0);
		
	}
	
	//Se carga el combo con los Roles
	private void LoadRoles() {
		
		//Se vacia el combo
		comboBoxRol.removeAllItems();
		
		//Se cargan los roles
		roles = usersBean.SelectRoles();
				
		//Se coloca un item indicador
		comboBoxRol.addItem("[Seleccione un Rol]");
		
		//Se colocan los roles en el combo
		for (Role role : roles) {
			comboBoxRol.addItem(role.getNombre());
		}
		
	}
	
	public UsersDialog() throws NamingException {
		setResizable(false);
		getContentPane().setBackground(new Color(0, 153, 102));
		setTitle("AgroDB: Formulario Usuarios");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(0, 102, 153));
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JSplitPane splitNombre = new JSplitPane();
			splitNombre.setBackground(new Color(0, 102, 153));
			splitNombre.setOrientation(JSplitPane.VERTICAL_SPLIT);
			contentPanel.add(splitNombre);
			{
				JLabel lblNombre = new JLabel("Nombre*:");
				lblNombre.setForeground(new Color(255, 255, 255));
				lblNombre.setFont(new Font("Noto Sans", Font.PLAIN, 14));
				lblNombre.setBackground(new Color(255, 255, 255));
				splitNombre.setLeftComponent(lblNombre);
			}
			{
				textFieldNombre = new JTextField();
				textFieldNombre.setFont(new Font("Noto Mono", Font.PLAIN, 12));
				textFieldNombre.setForeground(new Color(102, 102, 102));
				textFieldNombre.setBackground(new Color(255, 255, 255));
				textFieldNombre.setText("");
				textFieldNombre.setColumns(10);
				splitNombre.setRightComponent(textFieldNombre);
			}
		}
		{
			JSplitPane splitApellido = new JSplitPane();
			splitApellido.setBackground(new Color(0, 102, 153));
			splitApellido.setOrientation(JSplitPane.VERTICAL_SPLIT);
			contentPanel.add(splitApellido);
			{
				JLabel lblApellido = new JLabel("Apellido*:");
				lblApellido.setForeground(new Color(255, 255, 255));
				lblApellido.setFont(new Font("Noto Sans", Font.PLAIN, 14));
				lblApellido.setBackground(new Color(255, 255, 255));
				splitApellido.setLeftComponent(lblApellido);
			}
			{
				textFieldApellido = new JTextField();
				textFieldApellido.setFont(new Font("Noto Mono", Font.PLAIN, 12));
				textFieldApellido.setForeground(new Color(102, 102, 102));
				textFieldApellido.setBackground(new Color(255, 255, 255));
				textFieldApellido.setText("");
				textFieldApellido.setColumns(10);
				splitApellido.setRightComponent(textFieldApellido);
			}
		}
		{
			JSplitPane splitEmail = new JSplitPane();
			splitEmail.setBackground(new Color(0, 102, 153));
			splitEmail.setOrientation(JSplitPane.VERTICAL_SPLIT);
			contentPanel.add(splitEmail);
			{
				JLabel lblEmail = new JLabel("Email:");
				lblEmail.setForeground(new Color(255, 255, 255));
				lblEmail.setFont(new Font("Noto Sans", Font.PLAIN, 14));
				lblEmail.setBackground(new Color(255, 255, 255));
				splitEmail.setLeftComponent(lblEmail);
			}
			{
				textFieldEmail = new JTextField();
				textFieldEmail.setFont(new Font("Noto Mono", Font.PLAIN, 12));
				textFieldEmail.setForeground(new Color(102, 102, 102));
				textFieldEmail.setBackground(new Color(255, 255, 255));
				textFieldEmail.setText("");
				textFieldEmail.setColumns(10);
				splitEmail.setRightComponent(textFieldEmail);
			}
		}
		{
			JSplitPane splitUsuario = new JSplitPane();
			splitUsuario.setBackground(new Color(0, 102, 153));
			splitUsuario.setOrientation(JSplitPane.VERTICAL_SPLIT);
			contentPanel.add(splitUsuario);
			{
				JLabel lblUsuario = new JLabel("Usuario*:");
				lblUsuario.setForeground(new Color(255, 255, 255));
				lblUsuario.setFont(new Font("Noto Sans", Font.PLAIN, 14));
				lblUsuario.setBackground(new Color(255, 255, 255));
				splitUsuario.setLeftComponent(lblUsuario);
			}
			{
				textFieldUsuario = new JTextField();
				textFieldUsuario.setFont(new Font("Noto Mono", Font.PLAIN, 12));
				textFieldUsuario.setForeground(new Color(102, 102, 102));
				textFieldUsuario.setBackground(new Color(255, 255, 255));
				textFieldUsuario.setText("");
				textFieldUsuario.setColumns(10);
				splitUsuario.setRightComponent(textFieldUsuario);
			}
		}
		{
			JSplitPane splitContraseña = new JSplitPane();
			splitContraseña.setBackground(new Color(0, 102, 153));
			splitContraseña.setOrientation(JSplitPane.VERTICAL_SPLIT);
			contentPanel.add(splitContraseña);
			{
				JLabel lblContrasenia = new JLabel("Contrase\u00F1a*:");
				lblContrasenia.setForeground(new Color(255, 255, 255));
				lblContrasenia.setFont(new Font("Noto Sans", Font.PLAIN, 14));
				lblContrasenia.setBackground(new Color(255, 255, 255));
				splitContraseña.setLeftComponent(lblContrasenia);
			}
			{
				textFieldContrasenia = new JPasswordField();
				textFieldContrasenia.setFont(new Font("Noto Mono", Font.PLAIN, 12));
				textFieldContrasenia.setForeground(new Color(102, 102, 102));
				textFieldContrasenia.setBackground(new Color(255, 255, 255));
				splitContraseña.setRightComponent(textFieldContrasenia);
			}
		}
		{
			JSplitPane splitRol = new JSplitPane();
			splitRol.setBackground(new Color(0, 102, 153));
			splitRol.setOrientation(JSplitPane.VERTICAL_SPLIT);
			contentPanel.add(splitRol);
			{
				JLabel lblRol = new JLabel("Rol:");
				lblRol.setForeground(new Color(255, 255, 255));
				lblRol.setFont(new Font("Noto Sans", Font.PLAIN, 14));
				lblRol.setBackground(new Color(255, 255, 255));
				splitRol.setLeftComponent(lblRol);
			}
			{
				comboBoxRol = new JComboBox<String>();
				comboBoxRol.setFont(new Font("Noto Mono", Font.PLAIN, 12));
				comboBoxRol.setForeground(new Color(102, 102, 102));
				comboBoxRol.setBackground(new Color(255, 255, 255));
				comboBoxRol.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						if (comboBoxRol.getSelectedItem().equals("Experto")) {
							textFieldCedula.setEnabled(true);
							textFieldProfesion.setEnabled(true);
							textFieldInstituto.setEnabled(false);
						}
						else if (comboBoxRol.getSelectedItem().equals("Administrador")) {
							textFieldCedula.setEnabled(true);
							textFieldProfesion.setEnabled(false);
							textFieldInstituto.setEnabled(true);
						}
						else {
							textFieldCedula.setEnabled(false);
							textFieldProfesion.setEnabled(false);
							textFieldInstituto.setEnabled(false);
						}
					}
				});
				splitRol.setRightComponent(comboBoxRol);
			}
		}
		{
			JSplitPane splitCedula = new JSplitPane();
			splitCedula.setBackground(new Color(0, 102, 153));
			splitCedula.setOrientation(JSplitPane.VERTICAL_SPLIT);
			contentPanel.add(splitCedula);
			{
				JLabel lblCedula = new JLabel("C\u00E9dula:");
				lblCedula.setForeground(new Color(255, 255, 255));
				lblCedula.setFont(new Font("Noto Sans", Font.PLAIN, 14));
				lblCedula.setBackground(new Color(255, 255, 255));
				splitCedula.setLeftComponent(lblCedula);
			}
			{
				textFieldCedula = new JTextField();
				textFieldCedula.setFont(new Font("Noto Mono", Font.PLAIN, 12));
				textFieldCedula.setForeground(new Color(102, 102, 102));
				textFieldCedula.setBackground(new Color(255, 255, 255));
				textFieldCedula.setText("");
				textFieldCedula.setColumns(10);
				splitCedula.setRightComponent(textFieldCedula);
			}
		}
		{
			JSplitPane splitProfesion = new JSplitPane();
			splitProfesion.setBackground(new Color(0, 102, 153));
			splitProfesion.setOrientation(JSplitPane.VERTICAL_SPLIT);
			contentPanel.add(splitProfesion);
			{
				JLabel lblProfesion = new JLabel("Profesi\u00F3n:");
				lblProfesion.setForeground(new Color(255, 255, 255));
				lblProfesion.setFont(new Font("Noto Sans", Font.PLAIN, 14));
				lblProfesion.setBackground(new Color(255, 255, 255));
				splitProfesion.setLeftComponent(lblProfesion);
			}
			{
				textFieldProfesion = new JTextField();
				textFieldProfesion.setFont(new Font("Noto Mono", Font.PLAIN, 12));
				textFieldProfesion.setForeground(new Color(102, 102, 102));
				textFieldProfesion.setBackground(new Color(255, 255, 255));
				textFieldProfesion.setText("");
				textFieldProfesion.setColumns(10);
				splitProfesion.setRightComponent(textFieldProfesion);
			}
		}
		{
			JSplitPane splitInstituto = new JSplitPane();
			splitInstituto.setBackground(new Color(0, 102, 153));
			splitInstituto.setOrientation(JSplitPane.VERTICAL_SPLIT);
			contentPanel.add(splitInstituto);
			{
				JLabel lblInstituto = new JLabel("Instituto:");
				lblInstituto.setForeground(new Color(255, 255, 255));
				lblInstituto.setFont(new Font("Noto Sans", Font.PLAIN, 14));
				lblInstituto.setBackground(new Color(255, 255, 255));
				splitInstituto.setLeftComponent(lblInstituto);
			}
			{
				textFieldInstituto = new JTextField();
				textFieldInstituto.setFont(new Font("Noto Mono", Font.PLAIN, 12));
				textFieldInstituto.setForeground(new Color(102, 102, 102));
				textFieldInstituto.setBackground(new Color(255, 255, 255));
				textFieldInstituto.setText("");
				textFieldInstituto.setColumns(10);
				splitInstituto.setRightComponent(textFieldInstituto);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(new Color(0, 0, 153));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				btnCrear = new JButton("Crear");
				btnCrear.setBackground(new Color(255, 255, 255));
				btnCrear.setFont(new Font("Noto Sans", Font.PLAIN, 12));
				btnCrear.setForeground(new Color(102, 102, 102));
				btnCrear.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try { Action(); } catch (ServiciosException | ViewsException e) { JOptionPane.showMessageDialog(null, e.getMessage()); }
					}
				});
				buttonPane.add(btnCrear);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(new Color(0, 0, 153));
			getContentPane().add(buttonPane, BorderLayout.NORTH);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				lblTitle = new JLabel("Formulario de Alta de Usuarios");
				lblTitle.setForeground(new Color(255, 255, 255));
				lblTitle.setFont(new Font("Noto Sans", Font.PLAIN, 20));
				lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
				buttonPane.add(lblTitle);
			}
		}
		
		LoadRoles();
		
		CleanFields();
		
	}

}
