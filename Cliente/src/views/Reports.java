package views;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;

import exceptions.ViewsException;
import model.ActiCampo;
import model.MetoMuestreo;
import model.Role;
import services.ActiCampoBeanRemote;
import services.UsuariosBeanRemote;
import utilitis.XLS;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JScrollPane;

public class Reports {

	private JFrame frmAgrodbReportes;
	private JTable table;
	JComboBox<String> comboBoxMuestreos;
	JComboBox<String> comboBoxRol;
	JDateChooser dateChooserInicio;
	JDateChooser dateChooserFin;
	
	ActiCampoBeanRemote activitysBean = (ActiCampoBeanRemote) InitialContext.doLookup("AgroDB_Server/ActiCampoBean!services.ActiCampoBeanRemote");
	UsuariosBeanRemote usersBean = (UsuariosBeanRemote) InitialContext.doLookup("AgroDB_Server/UsuariosBean!services.UsuariosBeanRemote");
	
	List<ActiCampo> activitys;
	List<MetoMuestreo> methods;
	List<Role> roles;
	private JButton btnXLS;
	private JScrollPane scrollPane;
	
	public Reports() throws NamingException {
		initialize();
				
		LoadMethods();
		
		LoadRoles();
		
		LoadActivitys();
		
		frmAgrodbReportes.setVisible(true);
	}
	
	private void LoadActivitys() {
				
		//Se selecionan las actividades
		activitys = activitysBean.SelectActivitys();
						
		DefaultTableModel model = (DefaultTableModel) table.getModel();
			
		//Se vacia la tabla
        int rows = table.getRowCount();
        for (int i = rows-1; i >= 0; i--) {
        	model.removeRow(i);
        }
        
        //Se cargan las actividades
        for (ActiCampo activity : activitys) {					
        	model.addRow(new Object[]{
        			activity.getUsuario().getNombre(),
        			activity.getFecha(),
        			activity.getMetoMuestreo().getMetodo(),
        			activity.getEstaMuestreo().getEstacion()
        			});
        }	
		
	}
	
	private void LoadMethods() {
		
		//Se vacia el combo
		comboBoxMuestreos.removeAllItems();
		
		//Se selecionan los metodos
		methods = activitysBean.SelectMethods();
					        
		//Se colocan los metodos en el combo
        for (MetoMuestreo method : methods) {						
        	comboBoxMuestreos.addItem(method.getMetodo());
        }	
		
	}
	
	private void LoadRoles() {
		
		//Se vacia el combo
		comboBoxRol.removeAllItems();
		
		//Se selecionan los roles
		roles = usersBean.SelectRoles();
		
		//Se colocan los roles en el combo
		for (Role role : roles) {
			comboBoxRol.addItem(role.getNombre());
		}
		
	}
	
	private void Filter() throws ViewsException {
		
		//Se verifica que hayan actividades
		if (!(activitys.size() > 0))
			throw new ViewsException("Debe de haber actividades para poder filtrarlas");
			
		//Se verifican las fechas
		if(dateChooserInicio.getDate().compareTo(dateChooserFin.getDate()) > 0)
			throw new ViewsException("La fecha de inicio del filtrado no puede ser mayor a la de final");
		
		//Se selecionan la actividades filtradas
		activitys = activitysBean.SelectActivitys(methods.get(comboBoxMuestreos.getSelectedIndex()), roles.get(comboBoxRol.getSelectedIndex()), dateChooserInicio.getDate(), dateChooserFin.getDate());
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();
			
		//Se vacia la tabla
        int rows = table.getRowCount();
        for (int i = rows-1; i >= 0; i--) {
        	model.removeRow(i);
        }
        
        //Se cargan las actividades
        for (ActiCampo activity : activitys) {						
        	model.addRow(new Object[]{
        			activity.getUsuario().getNombre(),
        			activity.getFecha(),
        			activity.getMetoMuestreo().getMetodo(),
        			activity.getEstaMuestreo().getEstacion()
        			});
        }	
		
	}
	
	private void GenerateXLS() {
		
		//Se genera el XLS
		XLS.GenerateXLS("Analisis " + new Date().toString().replace(":", ""), table);
		
	}

	private void initialize() {
		frmAgrodbReportes = new JFrame();
		frmAgrodbReportes.setTitle("AgroDB: Reportes");
		frmAgrodbReportes.setBounds(100, 100, 800, 600);
		frmAgrodbReportes.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 153));
		frmAgrodbReportes.getContentPane().add(panel, BorderLayout.NORTH);
		
		comboBoxMuestreos = new JComboBox<String>();
		panel.add(comboBoxMuestreos);
		
		comboBoxRol = new JComboBox<String>();
		panel.add(comboBoxRol);
		
		dateChooserInicio = new JDateChooser();
		dateChooserInicio.setDate(new Date());
		panel.add(dateChooserInicio);
		
		dateChooserFin = new JDateChooser();
		dateChooserFin.setDate(new Date());
		panel.add(dateChooserFin);
		
		JButton btnNewButton = new JButton("Filtrar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Filter();
				} catch (ViewsException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		});
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Resetear");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LoadActivitys();
			}
		});
		panel.add(btnNewButton_1);
		
		btnXLS = new JButton("Generar .xls");
		btnXLS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GenerateXLS();
			}
		});
		panel.add(btnXLS);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Home.frame.setVisible(true);
				frmAgrodbReportes.dispose();
			}
		});
		frmAgrodbReportes.getContentPane().add(btnVolver, BorderLayout.SOUTH);
		
		scrollPane = new JScrollPane();
		frmAgrodbReportes.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Usuario", "Fecha", "Metodo Muestreo", "Estacion Muestreo"
			}
		));
	}

}
