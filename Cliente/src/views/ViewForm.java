package views;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.SystemColor;
import java.util.List;

import javax.swing.JTable;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;

import model.Casilla;
import model.Formulario;
import model.Tarea;
import services.CasillasBeanRemote;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class ViewForm {

	private JFrame frmAgrodbFormulario;
	private static JTable table;
	JTextPane txtpnNombreNombre;

	static Formulario viewingForm;

	static List<Casilla> boxs;
	private JButton btnVolver;
	static JButton btnLlenar;
	private JButton btnMuestreos;
	private JButton btnAgregarCasillaAFormulario;
	private JScrollPane scrollPane;
	
	public ViewForm(Formulario form, List<Tarea> tasks) throws NamingException {
		initialize();
		
		HideButtons(tasks);
		
		viewingForm = form;
		
		LoadForm();
		
		LoadBoxs();
		
		frmAgrodbFormulario.setVisible(true);
	}
	
	private void HideButtons(List<Tarea> tasks) {
		
		//Se oculta el boton de agregar casilla si no se tiene el rol
		for (Tarea task : tasks) {
			if (task.getNombre().equals("Gestion de Casillas")) {
				btnAgregarCasillaAFormulario.setVisible(true);
			}				
		}
		
	}

	private void LoadForm() {
		
		//Se carga los datos del formulario
		txtpnNombreNombre.setText("Nombre: " + viewingForm.getNombFormulario() + "."
				+ "Usuario Creador: " + viewingForm.getUsuario().getNombUsuario() + "."
				+ "Resumen: " + viewingForm.getResumen() + ".");
		
	}

	public static void LoadBoxs() throws NamingException {
		
		//Se obtienen las casillas
		CasillasBeanRemote boxsBean = (CasillasBeanRemote) InitialContext.doLookup("AgroDB_Server/CasillasBean!services.CasillasBeanRemote");
		boxs = boxsBean.SelectBoxs(viewingForm.getNombFormulario());
				
		DefaultTableModel model = (DefaultTableModel) table.getModel();
			
		//Se vacia la tabla
        int rows = table.getRowCount();
        for (int i = rows-1; i >= 0; i--) {
        	model.removeRow(i);
        }
          
        //Se a�aden las casillas obligatorias
        model.addRow(new Object[]{"Usuario", "", "Palabra", "Usuario quien realizo el Muestreo"});
        model.addRow(new Object[]{"Fecha", "", "Palabra", "Fecha en la que se realizo el Muestreo"});
        model.addRow(new Object[]{"Metodo de Muestreo", "", "Palabra", "Metodo por el cual se realizo el Muestreo"});
        model.addRow(new Object[]{"Estacion de Muestreo", "", "Palabra", "Estacion en la cual se realizo el Muestreo"});
        model.addRow(new Object[]{"Departamento", "", "Palabra", "Departamento en el cual se realizo el Muestreo"});
        
        //Para cada casilla
        for (Casilla box : boxs) {					
        	String unidad = "";
        	
        	//Se obitene la unidad
        	if (!(box.getUnidMedida() == null)) {
        		unidad = box.getUnidMedida().getNombre();
        	}
        	
        	//Se a�ade la casilla
        	model.addRow(new Object[]{
        			box.getParametro(),
        			unidad,
        			box.getTipoInput().getNombre(),
        			box.getDescripcion()
        			});
        }	
		
	}

	private void initialize() {
		frmAgrodbFormulario = new JFrame();
		frmAgrodbFormulario.setTitle("AgroDB: Ver Formulario");
		frmAgrodbFormulario.getContentPane().setBackground(new Color(0, 102, 153));
		frmAgrodbFormulario.setBounds(100, 100, 800, 600);
		frmAgrodbFormulario.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		txtpnNombreNombre = new JTextPane();
		txtpnNombreNombre.setText("Nombre: Nombre\r\nCreador: NombreUsuario\r\nResumen: Resumen");
		frmAgrodbFormulario.getContentPane().add(txtpnNombreNombre, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 204));
		frmAgrodbFormulario.getContentPane().add(panel, BorderLayout.SOUTH);
		
		btnLlenar = new JButton("Llenar");
		btnLlenar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					ActivitysDialog ad = new ActivitysDialog(viewingForm);
					ad.setModal(true);
					ad.setVisible(true); 
				} catch (NamingException e) {
					e.printStackTrace();
				} 
			}
		});
		btnLlenar.setForeground(SystemColor.textHighlight);
		btnLlenar.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel.add(btnLlenar);
		
		btnAgregarCasillaAFormulario = new JButton("Modificar Casillas");
		btnAgregarCasillaAFormulario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					CasillasList cl = new CasillasList(viewingForm);
					cl.setModal(true);
					cl.setVisible(true); 
				} catch (NamingException e) {
					e.printStackTrace();
				} 
			}
		});
		
		btnMuestreos = new JButton("Mustreos");
		btnMuestreos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					ActivitysList al = new ActivitysList(viewingForm);
					al.setModal(true);
					al.setVisible(true);
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
		});
		btnMuestreos.setForeground(SystemColor.textHighlight);
		btnMuestreos.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel.add(btnMuestreos);
		btnAgregarCasillaAFormulario.setForeground(SystemColor.textHighlight);
		btnAgregarCasillaAFormulario.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel.add(btnAgregarCasillaAFormulario);
		btnAgregarCasillaAFormulario.setVisible(false);
		
		btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					@SuppressWarnings("unused")
					FormulariosList f = new FormulariosList(Home.tasks);
					frmAgrodbFormulario.dispose();
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
		});
		btnVolver.setForeground(SystemColor.textHighlight);
		btnVolver.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel.add(btnVolver);
		
		scrollPane = new JScrollPane();
		frmAgrodbFormulario.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Parametro", "Unidad de Medida", "Tipo de Dato", "Descripcion"
			}
		));
		table.setBackground(Color.WHITE);
	}

}
