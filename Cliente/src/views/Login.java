package views;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import exceptions.ViewsException;
import model.Usuario;
import services.UsuariosBeanRemote;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login extends JDialog {
	
	private static final long serialVersionUID = 1L;
	
	private JTextField textFieldUsuario;
	private JPasswordField passwordField;
	
	private UsuariosBeanRemote usersBean = (UsuariosBeanRemote) InitialContext.doLookup("AgroDB_Server/UsuariosBean!services.UsuariosBeanRemote");
	
	@SuppressWarnings("deprecation")
	private void TryToLogIn() throws ViewsException, NamingException {
		
		//Si los campos estan nulos se coincidera no se quiere logear
		if (textFieldUsuario.getText() != null && passwordField.getText() != null) {
			
			//Se verifica los campos no esten vacios
			if (textFieldUsuario.getText().trim() == "" || passwordField.getText().trim() == "")
				throw new ViewsException("Debe de llenar ambos campos");
						
			//Se verifica el usuario exista
			Usuario user = usersBean.Login(textFieldUsuario.getText(), passwordField.getText());
			if (user == null)
				throw new ViewsException("El usuario y/o contraseņa es incorrecto");
			
			//Se configura el acceso de secciones en la ventana home
			Home.RoleConfiguration(user);
			
			//Se cierra el dialog
			dispose();
			
		}
		
	}
	
	public Login() throws NamingException {
		setResizable(false);
		setFont(new Font("Noto Sans", Font.PLAIN, 12));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().setBackground(new Color(0, 102, 204));
		setTitle("AgroDB: Login");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(null);
		
		JLabel lblImagen = new JLabel("");
		lblImagen.setIcon(new ImageIcon("D:\\Java\\Cliente\\AgroDB_Client\\Img\\UTECLogin.jpg"));
		lblImagen.setBounds(0, 0, 400, 571);
		getContentPane().add(lblImagen);
		
		JPanel panelButton = new JPanel();
		panelButton.setBackground(new Color(0, 0, 204));
		panelButton.setBounds(401, 471, 393, 100);
		getContentPane().add(panelButton);
		panelButton.setLayout(null);
		{
			JButton LoginButton = new JButton("Login");
			LoginButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {	// Evento del boton de Login
					try { TryToLogIn(); } catch (ViewsException | NamingException e) { JOptionPane.showMessageDialog(null, e.getMessage()); } 
				}
			});
			LoginButton.setFont(new Font("Noto Sans", Font.PLAIN, 20));
			LoginButton.setBounds(121, 24, 150, 50);
			panelButton.add(LoginButton);
			LoginButton.setForeground(new Color(0, 0, 0));
			LoginButton.setBackground(Color.WHITE);
			LoginButton.setActionCommand("Cancel");
		}
		
		JPanel panelData = new JPanel();
		panelData.setBackground(new Color(0, 102, 204));
		panelData.setBounds(401, 331, 393, 144);
		getContentPane().add(panelData);
		panelData.setLayout(null);
		{
			passwordField = new JPasswordField();
			passwordField.setEchoChar('*');
			passwordField.setFont(new Font("Noto Mono", Font.PLAIN, 20));
			passwordField.setBounds(142, 89, 241, 40);
			panelData.add(passwordField);
			passwordField.setBackground(Color.WHITE);
		}
		{
			JLabel lblContraseņa = new JLabel("Contrase\u00F1a:");
			lblContraseņa.setBounds(10, 100, 122, 17);
			panelData.add(lblContraseņa);
			lblContraseņa.setForeground(new Color(255, 255, 255));
			lblContraseņa.setFont(new Font("Noto Sans", Font.PLAIN, 20));
			lblContraseņa.setBackground(new Color(0, 153, 102));
		}
		{
			textFieldUsuario = new JTextField();
			textFieldUsuario.setFont(new Font("Noto Mono", Font.PLAIN, 20));
			textFieldUsuario.setBounds(142, 21, 241, 40);
			panelData.add(textFieldUsuario);
			textFieldUsuario.setColumns(10);
			textFieldUsuario.setBackground(Color.WHITE);
		}
		{
			JLabel lblUsuario = new JLabel("Usuario:");
			lblUsuario.setBounds(10, 32, 122, 17);
			panelData.add(lblUsuario);
			lblUsuario.setForeground(new Color(255, 255, 255));
			lblUsuario.setFont(new Font("Noto Sans", Font.PLAIN, 20));
			lblUsuario.setBackground(new Color(0, 153, 102));
		}
		
		JPanel panelTitleData = new JPanel();
		panelTitleData.setBackground(new Color(0, 0, 204));
		panelTitleData.setBounds(401, 278, 393, 55);
		getContentPane().add(panelTitleData);
		{
			JLabel lblTitleData = new JLabel("Login");
			lblTitleData.setForeground(Color.WHITE);
			lblTitleData.setFont(new Font("Noto Sans", Font.ITALIC, 32));
			panelTitleData.add(lblTitleData);
		}
		
		JPanel panelTitle = new JPanel();
		panelTitle.setBackground(new Color(0, 102, 204));
		panelTitle.setBounds(401, 0, 393, 277);
		getContentPane().add(panelTitle);
		panelTitle.setLayout(null);
		{
			JLabel lblProject = new JLabel("AgroDB");
			lblProject.setForeground(new Color(255, 255, 255));
			lblProject.setFont(new Font("Noto Sans", Font.ITALIC, 48));
			lblProject.setBounds(109, 103, 175, 92);
			panelTitle.add(lblProject);
		}
		{
			JLabel lblCourse = new JLabel("LTI - IAgro");
			lblCourse.setForeground(Color.WHITE);
			lblCourse.setFont(new Font("Noto Sans", Font.PLAIN, 32));
			lblCourse.setBounds(119, 179, 164, 71);
			panelTitle.add(lblCourse);
		}
		
		JLabel lblGroup = new JLabel("Binary Root");
		lblGroup.setForeground(Color.WHITE);
		lblGroup.setFont(new Font("Noto Sans", Font.BOLD | Font.ITALIC, 52));
		lblGroup.setBounds(51, 29, 295, 92);
		panelTitle.add(lblGroup);
	}
}
