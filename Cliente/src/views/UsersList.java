package views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import exceptions.ServiciosException;
import exceptions.ViewsException;

import java.awt.BorderLayout;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.List;

import model.Role;
import model.Usuario;
import services.UsuariosBeanRemote;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JScrollPane;

public class UsersList {
	
	private JFrame frmAgrodbUsuarios;
	private JTextField textFieldNombre;
	private JTextField textFieldApellido;
	private JTable table;
	JComboBox<String> comboBoxUser;
	JComboBox<String> comboBoxRol;
	
	UsuariosBeanRemote usersBean = (UsuariosBeanRemote) InitialContext.doLookup("AgroDB_Server/UsuariosBean!services.UsuariosBeanRemote");
	
	List<Usuario> users;
	List<Role> roles; 
	
	public UsersList() throws NamingException {
		
		initialize();
	
		LoadUsers();
		
		LoadRoles();
		
	}
	
	//Se carga la tabla con los Usuarios
	private void LoadUsers() {
				
		//Se obtienen los usuarios
		users = usersBean.SelectUsers();
						
		DefaultTableModel model = (DefaultTableModel) table.getModel();
			
		//Se vacia la tabla
        int rows = table.getRowCount();
        for (int i = rows-1; i >= 0; i--) {
        	model.removeRow(i);
        }
        
        //Se vacia el combo
        comboBoxUser.removeAllItems();
        
        //Se a�ade un item de indicacion
        comboBoxUser.addItem("[Seleccionar]");
        
        //Se a�aden los usuarios
        for (Usuario user : users) {						
        	model.addRow(new Object[]{ user.getNombUsuario(), user.getNombre(), user.getApellido(), user.getEmail(), user.getRole().getNombre(), user.getCedula(), user.getProfesion(), user.getInstituto() });
        	comboBoxUser.addItem(user.getNombUsuario());
        }	
		
	}
	
	private void LoadRoles() {
		
		//Se cargan los roles
		roles = usersBean.SelectRoles();
		
		//Se vacia el combo
		comboBoxRol.removeAllItems();
		
		//Se a�ade un item de indicacion
		comboBoxRol.addItem("[Seleccionar]");
		
		//Se a�aden los roles
		for (Role role : roles) {
			comboBoxRol.addItem(role.getNombre());
		}
		
	}
	
	
	//Se filtran los usuarios
	private void Filter() {

		//Se obtiene el nombre de usuario por el que filtrar
		String username;
		if (comboBoxUser.getSelectedIndex() == 0)
			username = "No";
		else
			username = comboBoxUser.getSelectedItem().toString();
		
		//Se obtiene el rol por el que filtrar
		Role role;
		if (comboBoxRol.getSelectedIndex() == 0)
			role = null;
		else
			role = roles.get(comboBoxRol.getSelectedIndex() - 1);
		
		//Se obtienen los usuarios filtrados
		users = usersBean.SelectUsers(username, textFieldNombre.getText(), textFieldApellido.getText(), role);
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		
		//Se vacia la tabla
        int rows = table.getRowCount();
        for (int i = rows-1; i >= 0; i--) {
        	model.removeRow(i);
        }
        
        //Se llena la tabla
        for (Usuario user : users) {						
        	model.addRow(new Object[]{ user.getNombUsuario(), user.getNombre(), user.getApellido(), user.getEmail(), user.getRole().getNombre(), user.getCedula(), user.getProfesion(), user.getInstituto() });
        }	
        
	}
	
	//Se borra el filtrado effectuado
	private void ClearFilter() {
		
		comboBoxUser.setSelectedIndex(0);
		textFieldNombre.setText("");
		textFieldApellido.setText("");
		
		LoadUsers();
		LoadRoles();
		
	}

	private void initialize() {
		frmAgrodbUsuarios = new JFrame();
		frmAgrodbUsuarios.setBackground(new Color(0, 102, 204));
		frmAgrodbUsuarios.getContentPane().setBackground(new Color(0, 102, 153));
		frmAgrodbUsuarios.setTitle("AgroDB: Usuarios");
		frmAgrodbUsuarios.setBounds(100, 100, 800, 600);
		frmAgrodbUsuarios.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JSplitPane splitTop = new JSplitPane();
		splitTop.setBackground(new Color(0, 102, 153));
		splitTop.setOrientation(JSplitPane.VERTICAL_SPLIT);
		frmAgrodbUsuarios.getContentPane().add(splitTop, BorderLayout.NORTH);
		
		JLabel lblTitle = new JLabel("Listado de Usuarios");
		lblTitle.setFont(new Font("Noto Sans", Font.PLAIN, 20));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setForeground(new Color(255, 255, 255));
		splitTop.setLeftComponent(lblTitle);
		
		JPanel panelFilters = new JPanel();
		panelFilters.setBackground(new Color(0, 0, 153));
		splitTop.setRightComponent(panelFilters);
		
		JSplitPane splitNombre = new JSplitPane();
		splitNombre.setBackground(new Color(0, 0, 153));
		splitNombre.setOrientation(JSplitPane.VERTICAL_SPLIT);
		panelFilters.add(splitNombre);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBackground(new Color(255, 255, 255));
		lblNombre.setForeground(new Color(255, 255, 255));
		lblNombre.setFont(new Font("Noto Sans", Font.PLAIN, 12));
		splitNombre.setLeftComponent(lblNombre);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setFont(new Font("Noto Mono", Font.PLAIN, 12));
		textFieldNombre.setForeground(new Color(102, 102, 102));
		textFieldNombre.setColumns(10);
		splitNombre.setRightComponent(textFieldNombre);
		
		JSplitPane splitApellido = new JSplitPane();
		splitApellido.setBackground(new Color(0, 0, 153));
		splitApellido.setOrientation(JSplitPane.VERTICAL_SPLIT);
		panelFilters.add(splitApellido);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setBackground(new Color(255, 255, 255));
		lblApellido.setForeground(new Color(255, 255, 255));
		lblApellido.setFont(new Font("Noto Sans", Font.PLAIN, 12));
		splitApellido.setLeftComponent(lblApellido);
		
		textFieldApellido = new JTextField();
		textFieldApellido.setFont(new Font("Noto Mono", Font.PLAIN, 12));
		textFieldApellido.setForeground(new Color(102, 102, 102));
		textFieldApellido.setColumns(10);
		splitApellido.setRightComponent(textFieldApellido);
		
		JSplitPane splitUsuario = new JSplitPane();
		splitUsuario.setBackground(new Color(0, 0, 153));
		splitUsuario.setOrientation(JSplitPane.VERTICAL_SPLIT);
		panelFilters.add(splitUsuario);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setBackground(new Color(255, 255, 255));
		lblUsuario.setForeground(new Color(255, 255, 255));
		lblUsuario.setFont(new Font("Noto Sans", Font.PLAIN, 12));
		splitUsuario.setLeftComponent(lblUsuario);
		
		comboBoxUser = new JComboBox<String>();
		comboBoxUser.setForeground(new Color(102, 102, 102));
		comboBoxUser.setFont(new Font("Noto Mono", Font.PLAIN, 12));
		splitUsuario.setRightComponent(comboBoxUser);
		
		JSplitPane splitRol = new JSplitPane();
		splitRol.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitRol.setBackground(new Color(0, 0, 153));
		panelFilters.add(splitRol);
		
		JLabel lblRol = new JLabel("Rol:");
		lblRol.setForeground(Color.WHITE);
		lblRol.setFont(new Font("Noto Sans", Font.PLAIN, 12));
		lblRol.setBackground(Color.WHITE);
		splitRol.setLeftComponent(lblRol);
		
		comboBoxRol = new JComboBox<String>();
		comboBoxRol.setForeground(new Color(102, 102, 102));
		comboBoxRol.setFont(new Font("Noto Mono", Font.PLAIN, 12));
		splitRol.setRightComponent(comboBoxRol);
		
		JSplitPane splitFiltersButtons = new JSplitPane();
		splitFiltersButtons.setBackground(new Color(0, 0, 153));
		splitFiltersButtons.setOrientation(JSplitPane.VERTICAL_SPLIT);
		panelFilters.add(splitFiltersButtons);
		
		JButton btnFiltrar = new JButton("Filtrar");
		btnFiltrar.setBackground(new Color(255, 255, 255));
		btnFiltrar.setForeground(new Color(102, 102, 102));
		btnFiltrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Filter();
			}
		});
		splitFiltersButtons.setLeftComponent(btnFiltrar);
		
		JButton btnResetear = new JButton("Resetear");
		btnResetear.setBackground(new Color(255, 255, 255));
		btnResetear.setForeground(new Color(102, 102, 102));
		btnResetear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ClearFilter();
			}
		});
		splitFiltersButtons.setRightComponent(btnResetear);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Home.frame.setVisible(true);
				frmAgrodbUsuarios.dispose();
			}
		});
		btnVolver.setForeground(new Color(102, 102, 102));
		btnVolver.setFont(new Font("Noto Sans", Font.PLAIN, 12));
		btnVolver.setBackground(Color.WHITE);
		frmAgrodbUsuarios.getContentPane().add(btnVolver, BorderLayout.SOUTH);
		
		JScrollPane scrollPane = new JScrollPane();
		frmAgrodbUsuarios.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setFont(new Font("Noto Mono", Font.PLAIN, 12));
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getButton() == MouseEvent.BUTTON3) { //Se detecta el boton derecho
					
					if(table.getSelectedRow() == -1)
						return;
					
					String [] botones = {"Modificar", "Eliminar"};					
					
					int opcion = JOptionPane.showOptionDialog (null, " Quieres desea hacer con el Usuario ' " + users.get(table.getSelectedRow()).getNombUsuario() + " '?",
							"Acciones", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.DEFAULT_OPTION, null, botones, botones[0]);
									
					if (opcion == 0) {	//Si elije modificar
						
						try {
							
							String[] data = new String[9];
							data[0] = users.get(table.getSelectedRow()).getNombre();
							data[1] = users.get(table.getSelectedRow()).getApellido();
							if(users.get(table.getSelectedRow()).getEmail() != null)
								data[2] = users.get(table.getSelectedRow()).getEmail();
							else
								data[2] = "";
							data[3] = users.get(table.getSelectedRow()).getNombUsuario();
							data[4] = users.get(table.getSelectedRow()).getContrasenia();
							if(users.get(table.getSelectedRow()).getCedula() != null)
								data[5] = users.get(table.getSelectedRow()).getCedula().toString();
							else
								data[5] = "";
							if(users.get(table.getSelectedRow()).getProfesion() != null)
								data[6] = users.get(table.getSelectedRow()).getProfesion();
							else
								data[6] = "";
							if(users.get(table.getSelectedRow()).getInstituto() != null)
								data[7] = users.get(table.getSelectedRow()).getInstituto();
							else
								data[7] = "";
							data[8] = users.get(table.getSelectedRow()).getRole().getNombre();
							
							UsersDialog u = new UsersDialog(); 
							
							u.SetUpdate(data);
							
							u.setModal(true);
							u.pack(); 
							u.setVisible(true); 
							
							LoadUsers();
							
						} catch (NamingException e1) { e1.printStackTrace(); }
						
					} else if (opcion == 1) { //Si elije eliminar
						
						try {
							
							if (users.get(table.getSelectedRow()).getIdUsuario() == Home.user.getIdUsuario())
								throw new ViewsException("No puedes darte de baja a ti mismo");
							
							if (users.get(table.getSelectedRow()).getRole().getNombre().equals("Administrador"))
								if (usersBean.GetCountAdmins(users.get(table.getSelectedRow()).getRole()) <= 1)
									throw new ViewsException("No puedes eliminar el ultimo administrador");
							
							if (usersBean.DeleteUser(users.get(table.getSelectedRow()).getIdUsuario())) {
							
								JOptionPane.showMessageDialog(null, "El usuario ha sido eliminado exitosamente");
								
								LoadUsers();
								
							}
														
						} catch (ServiciosException | ViewsException e1) { JOptionPane.showMessageDialog(null, e1.getMessage()); }
						
					}
					
				}
			}
		});
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Usuario", "Nombre", "Apellido", "Email", "Rol", "C\u00E9dula", "Profesi\u00F3n", "Instituto"
			}
		));
	
		frmAgrodbUsuarios.setVisible(true);
	}

}