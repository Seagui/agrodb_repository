package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import exceptions.ServiciosException;
import exceptions.ViewsException;
import model.TipoInput;
import model.UnidMedida;
import services.CasillasBeanRemote;

import javax.swing.JTextArea;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import java.util.List;

public class CasillasDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();
	JComboBox<String> comboBoxUnidadMedida;
	JComboBox<String> comboBoxTipoValor;
	JTextArea textAreaParametro;
	JTextArea textAreaDescripcion;
	JTextArea txtrParametro_1_2;
	JButton btnAgregar;

	CasillasBeanRemote boxsBean = (CasillasBeanRemote) InitialContext.doLookup("AgroDB_Server/CasillasBean!services.CasillasBeanRemote");
	
	List<TipoInput> inputs;
	List<UnidMedida> units;
	
	private void CrearCasilla() throws ViewsException, ServiciosException {
		
		//Se verifica que en el combo tipo valor no este seleccionado el mensaje informativo
		if (comboBoxTipoValor.getSelectedItem().equals(""))
			throw new ViewsException("Debes de llenar los campos obligatorios");
				
		//Se verifica que en el combo no este seleccionado el mensaje informativo si el tipo valor no es palabra
		if (!(comboBoxTipoValor.getSelectedItem().equals("Palabra")) && comboBoxUnidadMedida.getSelectedItem().toString().equals("")) 
			throw new ViewsException("Debes de llenar los campos obligatorios");
		
		//Si los campos no estan nulos, se interpreta se quiere ingresar algo
		if (textAreaParametro.getText() != null) {
		
			//Se verifica los campos no esten vacios
			if (textAreaParametro.getText().trim().isEmpty())
				throw new ViewsException("Debes de llenar los campos obligatorios");
											
			UnidMedida unidad = null;
			
			if (!(comboBoxTipoValor.getSelectedItem().equals("Palabra"))) {
				unidad = units.get(comboBoxUnidadMedida.getSelectedIndex());
			}
				
			if (boxsBean.CreateBox(textAreaParametro.getText(), textAreaDescripcion.getText(), inputs.get(comboBoxTipoValor.getSelectedIndex()), unidad)) {
				
				JOptionPane.showMessageDialog(null, "La casilla ha sido creada exitosamente");
				
				CleanFields();
				
			}
			
		}		
		
	}
	
	private void CleanFields() {
		
		//Se vacian los campos
		textAreaParametro.setText("");
		textAreaDescripcion.setText("");
		comboBoxTipoValor.setSelectedIndex(0);
		comboBoxUnidadMedida.setSelectedIndex(0);
		
	}
	
	private void LoadUnits() {
		
		//Se vacia el combo
		comboBoxUnidadMedida.removeAllItems();
		
		//Se seleccionan las casillas
		units = boxsBean.SelectUnits();
									
		//Se carga el combo
		for (UnidMedida unit : units) {
			comboBoxUnidadMedida.addItem(unit.getNombre());
		}
		
	}
	
	private void LoadInputs() {
		
		//Se vacia el combo
		comboBoxTipoValor.removeAllItems();
		
		//Se seleccioan los inputs
		inputs = boxsBean.SelectInputs();
									
		//Se carga el combo
		for (TipoInput input : inputs) {
			comboBoxTipoValor.addItem(input.getNombre());
		}
		
	}
	
	private void CreateUnit() throws ServiciosException, ViewsException {
		
		String name = JOptionPane.showInputDialog("Ingrese la Unidad de Medida");
		
		//Si el nombre no es nulo, se interpreta quiere ingresar algo
		if (name != null) {
			
			//Se verifica que el nombre no este vacio
			if (name.trim().equals(""))
				throw new ViewsException("Debe de ingresar algo en el campo");
												
			if (boxsBean.CreateUnit(name)) {
				
				JOptionPane.showMessageDialog(null, "La unidad de medida ha sido creada exitosamente");
				
				//Se actualiza el ComboBox con la nueva Unidad de Medida
				LoadUnits();
				
			}
												
		}
		
	}
	
	/**
	 * Create the dialog.
	 */
	public CasillasDialog() throws NamingException {
		setResizable(false);
		getContentPane().setBackground(new Color(0, 102, 153));
		setBackground(new Color(0, 102, 153));
		setTitle("AgroDB: Crear Casilla");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(0, 0, 153));
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		{
			JTextArea txtrCrearCasillaNueva = new JTextArea();
			txtrCrearCasillaNueva.setText("CREAR CASILLA NUEVA :");
			txtrCrearCasillaNueva.setFont(new Font("Arial Black", Font.PLAIN, 15));
			contentPanel.add(txtrCrearCasillaNueva);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(new Color(0, 0, 153));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Crear");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try {
							CrearCasilla();
						} catch (ViewsException | ServiciosException e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		{
			JPanel contentPanel_1 = new JPanel();
			contentPanel_1.setBackground(new Color(0, 102, 153));
			contentPanel_1.setBorder(new EmptyBorder(5, 5, 5, 5));
			getContentPane().add(contentPanel_1, BorderLayout.CENTER);
			contentPanel_1.setLayout(new FlowLayout());
			{
				JTextArea txtrParametro_1 = new JTextArea();
				txtrParametro_1.setText("* PARAMETRO :");
				txtrParametro_1.setForeground(new Color(0, 0, 0));
				txtrParametro_1.setFont(new Font("Arial Black", Font.PLAIN, 15));
				contentPanel_1.add(txtrParametro_1);
			}
			{
				textAreaParametro = new JTextArea();
				textAreaParametro.setColumns(20);
				textAreaParametro.setForeground(Color.BLACK);
				textAreaParametro.setBackground(new Color(255, 255, 255));
				contentPanel_1.add(textAreaParametro);
			}
			{
				JPanel panel = new JPanel();
				contentPanel_1.add(panel);
				{
					JTextArea txtrParametro_1_3 = new JTextArea();
					txtrParametro_1_3.setText("* TIPO VALOR:");
					txtrParametro_1_3.setForeground(new Color(0, 0, 0));
					txtrParametro_1_3.setFont(new Font("Arial Black", Font.PLAIN, 12));
					panel.add(txtrParametro_1_3);
				}
				{
					comboBoxTipoValor = new JComboBox<String>();
					comboBoxTipoValor.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							if (comboBoxTipoValor.getSelectedItem().equals("Palabra")) {
								comboBoxUnidadMedida.setVisible(false);
								txtrParametro_1_2.setVisible(false);
								btnAgregar.setVisible(false);
							} else {
								comboBoxUnidadMedida.setVisible(true);
								txtrParametro_1_2.setVisible(true);	
								btnAgregar.setVisible(true);
							}
						}
					});
					panel.add(comboBoxTipoValor);
				}
			}
			{
				txtrParametro_1_2 = new JTextArea();
				txtrParametro_1_2.setText("* UNIDAD DE MEDIDA:");
				txtrParametro_1_2.setForeground(new Color(0, 0, 0));
				txtrParametro_1_2.setFont(new Font("Arial Black", Font.PLAIN, 15));
				contentPanel_1.add(txtrParametro_1_2);
			}
			{
				comboBoxUnidadMedida = new JComboBox<String>();
				contentPanel_1.add(comboBoxUnidadMedida);
			}
			{
				btnAgregar = new JButton("Agregar");
				btnAgregar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try {
							CreateUnit();
						} catch (ServiciosException | ViewsException e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
					}
				});
				contentPanel_1.add(btnAgregar);
			}
			{
				JTextArea txtrParametro_1_1 = new JTextArea();
				txtrParametro_1_1.setText("DESCRIPCION:");
				txtrParametro_1_1.setForeground(new Color(0, 0, 0));
				txtrParametro_1_1.setFont(new Font("Arial Black", Font.PLAIN, 15));
				contentPanel_1.add(txtrParametro_1_1);
			}
			{
				textAreaDescripcion = new JTextArea();
				textAreaDescripcion.setRows(2);
				textAreaDescripcion.setColumns(20);
				textAreaDescripcion.setForeground(Color.BLACK);
				textAreaDescripcion.setBackground(new Color(255, 255, 255));
				contentPanel_1.add(textAreaDescripcion);
			}
		}
		
		LoadUnits();
		
		LoadInputs();
		
	}

}
