package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.HeadlessException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import exceptions.ServiciosException;
import exceptions.ViewsException;
import model.ActiCampo;
import model.Casilla;
import model.EstaMuestreo;
import model.Formulario;
import model.MetoMuestreo;
import model.Zona;
import services.ActiCampoBeanRemote;
import services.CasillasBeanRemote;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import java.awt.Color;
import com.toedter.calendar.JDateChooser;

public class ActivitysDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();
	JComboBox<String> comboBoxMetodo;
	JComboBox<String> comboBoxEstacion;
	JComboBox<String> comboBoxZona;
	JDateChooser dateChooser;

	List<MetoMuestreo> methods;
	List<EstaMuestreo> stations;
	List<Zona> zones;
	List<Casilla> boxs;
	List<JComponent> components;
	
	ActiCampoBeanRemote activitysBean = (ActiCampoBeanRemote) InitialContext.doLookup("AgroDB_Server/ActiCampoBean!services.ActiCampoBeanRemote");
	CasillasBeanRemote boxsBean = (CasillasBeanRemote) InitialContext.doLookup("AgroDB_Server/CasillasBean!services.CasillasBeanRemote");
	
	private void LoadMethods() {
		
		//Se vacia el combo
		comboBoxMetodo.removeAllItems();
		
		//Se obtienen los metodos
		methods = activitysBean.SelectMethods();
					        
		//Se a�aden los metodos al combo
        for (MetoMuestreo method : methods) {						
        	comboBoxMetodo.addItem(method.getMetodo());
        }	
		
	}
	
	private void CreateMethod() throws HeadlessException, ServiciosException, ViewsException {
		
		String method = JOptionPane.showInputDialog("Ingrese el Metodo");
		
		//Si el nombre no es nulo, se interpreta quiere ingresar algo
		if (method != null) {
			
			//Se verifica que el nombre no este vacio
			if (method.trim().equals(""))
				throw new ViewsException("Debe de ingresar algo en el campo");
												
			if (activitysBean.CreateMethods(method)) {
				
				JOptionPane.showMessageDialog(null, "El Metodo de Muestreo ha sido creado exitosamente");
				
				//Se actualiza el ComboBox con la nueva Unidad de Medida
				LoadMethods();
				
			}
												
		}
		
	}
	
	private void LoadStations() {
		
		//Se vacia el combo
		comboBoxEstacion.removeAllItems();
		
		//Se obtienen las estaciones
		stations = activitysBean.SelectStations();
        
		//Se a�aden las estaciones al combo
        for (EstaMuestreo station : stations) {						
        	comboBoxEstacion.addItem(station.getEstacion());
        }	
		
	}
	
	private void CreateStation() throws HeadlessException, ServiciosException, ViewsException {
		
		String station = JOptionPane.showInputDialog("Ingrese la Estacion");
		
		//Si el nombre no es nulo, se interpreta quiere ingresar algo
		if (station != null) {
			
			//Se verifica que el nombre no este vacio
			if (station.trim().equals(""))
				throw new ViewsException("Debe de ingresar algo en el campo");
												
			if (activitysBean.CreateStation(station)) {
				
				JOptionPane.showMessageDialog(null, "La Estacion de Muestreo ha sido creada exitosamente");
				
				//Se actualiza el ComboBox con la nueva Unidad de Medida
				LoadStations();
				
			}
												
		}
		
	}
	
	private void LoadZones() {
		
		//Se vacia el combo
		comboBoxZona.removeAllItems();
		
		//Se obtienen las zonas
		zones = activitysBean.SelectZones();
        
		//Se a�aden las zonas al combo
        for (Zona zone : zones) {						
        	comboBoxZona.addItem(zone.getZona());
        }	
		
	}
	
	private void CreateZone() throws HeadlessException, ServiciosException, ViewsException {
		
		String zone = JOptionPane.showInputDialog("Ingrese la Zona");
		
		//Si el nombre no es nulo, se interpreta quiere ingresar algo
		if (zone != null) {
			
			//Se verifica que el nombre no este vacio
			if (zone.trim().equals(""))
				throw new ViewsException("Debe de ingresar algo en el campo");
												
			if (activitysBean.CreateZone(zone)) {
				
				JOptionPane.showMessageDialog(null, "La Zona ha sido creada exitosamente");
				
				//Se actualiza el ComboBox con la nueva Unidad de Medida
				LoadZones();
				
			}
												
		}
		
	}
	
	private void LoadBoxs(String formName) {
		
		//Se obtienen las casillas
		components = new ArrayList<JComponent>();
		boxs = boxsBean.SelectBoxs(formName);
		
		//Por cada casilla...
		for (Casilla box : boxs) {
			
			//Se crea un panel para colocarla
			JPanel panelBox = new JPanel();
			panelBox.setBackground(new Color(0, 102, 153));
			
			//Se crea el label indicador de casilla
			JLabel labelBox = new JLabel();
			if (box.getTipoInput().getNombre().equals("Palabra")) {
				labelBox.setText(box.getParametro() + ":");
			}
			else {
				labelBox.setText(box.getParametro() + " (" + box.getUnidMedida().getNombre() + "):");
			}
			labelBox.setForeground(new Color(255, 255, 255));
			panelBox.add(labelBox);
			
			//Se a�ade el componente para el valor
			if (box.getTipoInput().getNombre().equals("Entero") || box.getTipoInput().getNombre().equals("Palabra") || box.getTipoInput().getNombre().equals("Decimal")) {
				JTextField textFieldBox = new JTextField();
				textFieldBox.setColumns(10);
				panelBox.add(textFieldBox);
				components.add(textFieldBox);
			}
			else if (box.getTipoInput().getNombre().equals("Verdadero/Falso")) {
				JCheckBox checkBoxBox = new JCheckBox();
				panelBox.add(checkBoxBox);
				components.add(checkBoxBox);
			}
		
			//Se a�ade el panel a el panel contenedor
			contentPanel.add(panelBox);
		}
		
	}
	
	private void CreateActivity(Formulario form) throws ViewsException, HeadlessException, ServiciosException {
		
		//Se verifica que en el combo no este seleccionado el mensaje informativo
		if (comboBoxMetodo.getSelectedItem().toString().equals("") || comboBoxEstacion.getSelectedItem().equals("") || comboBoxZona.getSelectedItem().equals(""))
			throw new ViewsException("Debes de llenar los campos obligatorios");	
				
		//Para cada casilla
		int i = 0;
		List<String> valores = new ArrayList<String>();
		for (Casilla box : boxs) {
			
			String valor = "";
			
			if (box.getTipoInput().getNombre().equals("Entero")) {	//Si es un entero

				JTextField momentary = (JTextField) components.get(i);
				valor = momentary.getText();
				
				//Se verifica sea un entero
				try {
					@SuppressWarnings("unused")
					int entero = Integer.parseInt(valor);
				} catch (Exception e) {
					throw new ViewsException("El parametro '" + box.getParametro() + "' solo acepta numeros enteros");
				}
								
			}
			else if (box.getTipoInput().getNombre().equals("Palabra")) {	//Si es una palabra
				
				JTextField momentary = (JTextField) components.get(i);
				valor = momentary.getText();
				
			}
			else if (box.getTipoInput().getNombre().equals("Decimal")) {	//Si es un decimal
			
				JTextField momentary = (JTextField) components.get(i);
				valor = momentary.getText();
				
				//Se verifica sea un decimal
				try {
					@SuppressWarnings("unused")
					double entero = Double.parseDouble(valor);
				} catch (Exception e) {
					throw new ViewsException("El parametro '" + box.getParametro() + "' solo acepta numeros enteros y decimales");
				}

			}
			else if (box.getTipoInput().getNombre().equals("Verdadero/Falso")) {	//Si es verdadero o falso

				JCheckBox momentary = (JCheckBox) components.get(i);
				valor = Boolean.toString(momentary.isSelected());
				
			}

			//Se a�ade el valor en el componente
			valores.add(valor);
			
			i++;
		}
		
		//Se crea la actividad
		if (activitysBean.CreateActivity(dateChooser.getDate(), Home.user, form, methods.get(comboBoxMetodo.getSelectedIndex()), stations.get(comboBoxEstacion.getSelectedIndex()), zones.get(comboBoxZona.getSelectedIndex()))) {
			
			JOptionPane.showMessageDialog(null, "La Actividad ha sido creada exitosamente");
						
		}
				
		//Se crean los datos de la actividad
		ActiCampo activity = activitysBean.SelectActivity(dateChooser.getDate(), Home.user, form);
		i = 0;
		for (String valor : valores) {
			
			activitysBean.CreateData(activity, boxs.get(i), valor);
			
			i++;
		}
		
	}
	
	public ActivitysDialog(Formulario form) throws NamingException {
		setResizable(false);
		getContentPane().setBackground(new Color(0, 102, 153));
		getContentPane().setForeground(new Color(0, 102, 153));
		setTitle("AgroDB: Crear Actividad");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(0, 102, 153));
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel panelMetodo = new JPanel();
			panelMetodo.setBackground(new Color(0, 102, 153));
			contentPanel.add(panelMetodo);
			{
				JLabel lblNewLabel = new JLabel("Metodo de Muestreo*:");
				lblNewLabel.setForeground(new Color(255, 255, 255));
				panelMetodo.add(lblNewLabel);
			}
			{
				comboBoxMetodo = new JComboBox<String>();
				panelMetodo.add(comboBoxMetodo);
			}
			{
				JButton btnMetodo = new JButton("Agregar");
				btnMetodo.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try {
							CreateMethod();
						} catch (HeadlessException | ServiciosException | ViewsException e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
					}
				});
				panelMetodo.add(btnMetodo);
			}
		}
		{
			JPanel panelEstacion = new JPanel();
			panelEstacion.setBackground(new Color(0, 102, 153));
			contentPanel.add(panelEstacion);
			{
				JLabel lblNewLabel_1 = new JLabel("Estacion de Muestreo*:");
				lblNewLabel_1.setForeground(new Color(255, 255, 255));
				panelEstacion.add(lblNewLabel_1);
			}
			{
				comboBoxEstacion = new JComboBox<String>();
				panelEstacion.add(comboBoxEstacion);
			}
			{
				JButton btnEstacion = new JButton("Agregar");
				btnEstacion.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try {
							CreateStation();
						} catch (HeadlessException | ServiciosException | ViewsException e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
					}
				});
				panelEstacion.add(btnEstacion);
			}
		}
		{
			JPanel panelZona = new JPanel();
			panelZona.setBackground(new Color(0, 102, 153));
			contentPanel.add(panelZona);
			{
				JLabel lblNewLabel_2 = new JLabel("Zona*:");
				lblNewLabel_2.setForeground(new Color(255, 255, 255));
				panelZona.add(lblNewLabel_2);
			}
			{
				comboBoxZona = new JComboBox<String>();
				panelZona.add(comboBoxZona);
			}
			{
				JButton buttonZona = new JButton("Agregar");
				buttonZona.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try {
							CreateZone();
						} catch (HeadlessException | ServiciosException | ViewsException e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
					}
				});
				panelZona.add(buttonZona);
			}
		}
		{
			JPanel panelFecha = new JPanel();
			panelFecha.setBackground(new Color(0, 102, 153));
			panelFecha.setForeground(new Color(0, 102, 153));
			contentPanel.add(panelFecha);
			{
				JLabel lblFecha = new JLabel("Fecha*:");
				lblFecha.setForeground(new Color(255, 255, 255));
				panelFecha.add(lblFecha);
			}
			{
				dateChooser = new JDateChooser();
				dateChooser.setDate(new Date());
				panelFecha.add(dateChooser);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(new Color(0, 0, 153));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnCrear = new JButton("Registrar");
				btnCrear.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try {
							CreateActivity(form);
						} catch (ViewsException | HeadlessException | ServiciosException e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
					}
				});
				btnCrear.setActionCommand("OK");
				buttonPane.add(btnCrear);
				getRootPane().setDefaultButton(btnCrear);
			}
		}
		
		LoadMethods();
		
		LoadStations();
		
		LoadZones();
		
		LoadBoxs(form.getNombFormulario());
		
	}

}
