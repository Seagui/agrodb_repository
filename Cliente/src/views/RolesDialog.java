package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JSplitPane;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.BoxLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import exceptions.ServiciosException;
import exceptions.ViewsException;
import model.Role;
import model.Tarea;
import services.UsuariosBeanRemote;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JScrollPane;

public class RolesDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private JTable table;
	JComboBox<String> comboBoxRoles;

	UsuariosBeanRemote usersBean = (UsuariosBeanRemote) InitialContext.doLookup("AgroDB_Server/UsuariosBean!services.UsuariosBeanRemote");
	
	List<Role> roles;
	List<Tarea> tasks;
	
		//Se cargan los Roles al ComboBox
		private void LoadRoles() {
			
			comboBoxRoles.removeAllItems();
			
			roles = usersBean.SelectRoles();
					
			comboBoxRoles.addItem("[Seleccione un Rol]");
						
			for (Role role : roles) {
				comboBoxRoles.addItem(role.getNombre());
			}
									
		}
		
		//Se crea un Rol
		private void CreateRole() throws ServiciosException, ViewsException {
							
			String name = JOptionPane.showInputDialog("Ingrese el nombre del Rol");
			
			//Si el nombre no es nulo, se interpreta quiere ingresar algo
			if (name != null) {
				
				//Se verifica que el nombre no este vacio
				if (name.isEmpty())
					throw new ViewsException("El nombre no puede estar vacio");
										
				if (usersBean.CreateRole(name)) {
				
					JOptionPane.showMessageDialog(null, "El rol se ha creado con exito");
					
					//Se actualiza el ComboBox con el nuevo Rol
					LoadRoles();
					
				}
								
			}
			
		}
		
		//Se modifica un Rol
		private void UpdateRole() throws ServiciosException, ViewsException {
			
			//Se verifica que en el combo no este seleccionado el mensaje informativo
			if (comboBoxRoles.getSelectedIndex() == 0) 
				throw new ViewsException("No puedes modificar el mensaje informativo");
			
			if (comboBoxRoles.getSelectedItem().toString().equals("Administrador") || comboBoxRoles.getSelectedItem().toString().equals("Experto") || comboBoxRoles.getSelectedItem().toString().equals("Com�n"))
				throw new ViewsException("No puedes modificar este rol, porque es necesario para el sistema");
				
			int index = comboBoxRoles.getSelectedIndex() - 1;	
			String newName = JOptionPane.showInputDialog("Ingrese el nuevo nombre del Rol: " + roles.get(index).getNombre());
				
			//Si el nombre no es nulo, se interpreta quiere ingresar algo
			if (newName != null) {
					
				//Se verifica que el nombre no este vacio
				if (newName.isEmpty())
					throw new ViewsException("El nombre no puede estar vacio");				
												
				if (usersBean.UpdateRole(newName, roles.get(index).getIdRol())) {
				
					JOptionPane.showMessageDialog(null, "El Rol ha sido modificado correctamente");
					
					//Se actualiza el ComboBox con el Rol modificado
					LoadRoles();
					
				}
									
			}
			
		}
		
		//Se limina un Rol
		private void DeleteRole() throws ServiciosException, ViewsException {
			
			//Se verifica que en el combo no este seleccionado el mensaje informativo
			if (comboBoxRoles.getSelectedIndex() == 0) 
				throw new ViewsException("No puedes eliminar el mensaje informativo");
			
			if (comboBoxRoles.getSelectedItem().toString().equals("Administrador") || comboBoxRoles.getSelectedItem().toString().equals("Experto") || comboBoxRoles.getSelectedItem().toString().equals("Com�n"))
				throw new ViewsException("No puedes eliminar este rol, porque es necesario para el sistema");
				
			int index = comboBoxRoles.getSelectedIndex() - 1;
				
			if (JOptionPane.showConfirmDialog(null, "Seguro que desea eliminar el Rol: " + roles.get(index).getNombre()) == 0) {
																	
				if (usersBean.DeleteRole(roles.get(index).getIdRol())) {
					
					JOptionPane.showMessageDialog(null, "El Rol ha sido modificado correctamente");
					
					//Se actualiza el ComboBox con el Rol modificado
					LoadRoles();
					
				}
					
			}
					
		}
		
		//Se cargan las Tareas a la tabla
		private void LoadTasks() {
			
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			
			//Se vacia la tabla
            int rows = table.getRowCount();
            for (int i = rows-1; i >= 0; i--) {
                model.removeRow(i);
            }

			
			//Solo se cargan la tabla si hay un rol seleccionado en el combo
			if (comboBoxRoles.getSelectedIndex() != 0 && comboBoxRoles.getItemCount() > 0) {
				
				int index = comboBoxRoles.getSelectedIndex() - 1;
				tasks = usersBean.SelectTasks(roles.get(index).getNombre());
											
				for (Tarea task : tasks) {
					model.addRow(new Object[]{task.getNombre()});
				}
				
			}
			
		}
		
		//Se crea una Tarea
		private void CreateTask() throws ServiciosException, ViewsException {
			
			//Se verifica que en el combo no este seleccionado el mensaje informativo
			if (comboBoxRoles.getSelectedIndex() == 0)
				throw new ViewsException("No puedes crear una tarea al mensaje informativo");
				
			int option = JOptionPane.showConfirmDialog(null, "Desea crear una nueva Tarea?");
			if(option == 0) {	//Si contesta que S�
					
				String name = JOptionPane.showInputDialog("Ingrese el nombre de la Tarea");
					
				//Si el nombre no es nulo, se interpreta quiere ingresar algo
				if (name != null) {
						
					//Se verifica que el nombre no este vacio
					if (name.isEmpty())
						throw new ViewsException("El nombre no puede estar vacio");
							
					int index = comboBoxRoles.getSelectedIndex() - 1;
					if (usersBean.CreateRoleTask(name, roles.get(index)))
						JOptionPane.showMessageDialog(null, "La tarea ha sido creada y asignada exitosamente");
																								
				}
									
			}
			else if(option == 1) {	//Si contesta que No
					
				List<Tarea> allTasks = usersBean.SelectTasks();
					
				//Se verifica que hayan tareas
				if(allTasks.size() <= 0) 
					throw new ViewsException("No hay tareas creadas");
						
				//Se colocan en un combo que se muestra en un dialogo todas las Tareas
				JComboBox<String> comboBoxTareas = new JComboBox<String>();
				for (Tarea task : allTasks) {
					comboBoxTareas.addItem(task.getNombre());
				}
				int opc = JOptionPane.showConfirmDialog(null, comboBoxTareas);
				
				if (opc != -1) {
										
					int index = comboBoxRoles.getSelectedIndex() - 1;
					
					for (Tarea task : usersBean.SelectTasks(roles.get(index).getNombre())) {
						
						if (task.getNombre().equals(allTasks.get(comboBoxTareas.getSelectedIndex()).getNombre()))
							throw new ViewsException("La tarea ya esta asignada al Rol");				
						
					}
					
					if (usersBean.CreateRoleTask(allTasks.get(comboBoxTareas.getSelectedIndex()), roles.get(index)))
						JOptionPane.showMessageDialog(null, "La tarea ha sido asignada exitosamente");
					
				}
				
			}
				
			//Se refrescan las Tareas de la tabla con la nueva
			LoadTasks();

		}
		
		//Se modifica una Tarea
		private void UpdateTask() throws ServiciosException, ViewsException {
					
			//Se verifica que en el combo no este seleccionado el mensaje informativo
			if(table.getSelectedRow() == -1)
				throw new ViewsException("Debes de selccionar una tarea");
			
			if (tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Usuarios") || tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Roles") ||
					tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Tareas") || tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Formularios") ||
					tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Casillas") || tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Actividades") ||
					tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Reportes"))
				throw new ViewsException("No puedes modificar esta tarea, porque es necesaria para el sistema");
				
			String newName = JOptionPane.showInputDialog("Ingrese el nuevo nombre de la Tarea: " + tasks.get(table.getSelectedRow()).getNombre());
				
			if (newName != null) {
					
				//Se verifica que en el el nombre no este vacio
				if (newName.isEmpty())
					throw new ViewsException("El nombre no puede estar vacio");
																				
				if (usersBean.UpdateTask(newName, tasks.get(table.getSelectedRow()).getIdTarea())) {

					JOptionPane.showMessageDialog(null, "La tarea ha sido modificada correctamente");
					
					//Se refesca la tabla con la tarea modificada
					LoadTasks();

				}
									
			}
										
		}
		
		//Se elimina una Tarea
		private void DeleteTask() throws ServiciosException, ViewsException {
			
			//Se verifica que en el combo no este seleccionado el mensaje informativo
			if (table.getSelectedRow() == -1)
				throw new ViewsException("Debes de seleccionar una tarea");
				
			if (comboBoxRoles.getSelectedItem().toString().equals("Administrador") || comboBoxRoles.getSelectedItem().toString().equals("Experto") || comboBoxRoles.getSelectedItem().toString().equals("Com�n"))
				if (comboBoxRoles.getSelectedItem().toString().equals("Administrador"))
					if (tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Usuarios") || tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Roles") ||
							tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Tareas") || tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Formularios") ||
							tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Casillas") || tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Actividades") ||
							tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Reportes"))
						throw new ViewsException("No puedes eliminar esta tarea, porque es necesaria para el sistema");
				else if (comboBoxRoles.getSelectedItem().toString().equals("Experto"))
					if (tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Formularios") || tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Casillas") ||
							tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Actividades"))
						throw new ViewsException("No puedes eliminar esta tarea, porque es necesaria para el sistema");
				else
					if (tasks.get(table.getSelectedRow()).getNombre().equals("Gestion de Actividades"))
						throw new ViewsException("No puedes eliminar esta tarea, porque es necesaria para el sistema");
					
			
			if (usersBean.GetCountTasks(tasks.get(table.getSelectedRow())) > 1) {	//Si la tarea esta en mas relaciones
				
				if (JOptionPane.showConfirmDialog(null, "Seguro que desea eliminar la Relacion de la Tarea: " + tasks.get(table.getSelectedRow()).getNombre()) == 0) {
					
					if (usersBean.DeleteRoleTask(tasks.get(table.getSelectedRow()), roles.get(comboBoxRoles.getSelectedIndex() - 1))) {
						
						JOptionPane.showMessageDialog(null, "La tarea ha sido quitada del rol exiotasemente");
						
						//Se refresca la tabla con la relacion eliminada
						LoadTasks();
						
					}			
						
				}
				
			} else {	//Si es la unica relacion de la tarea
			
				if (JOptionPane.showConfirmDialog(null, "Seguro que desea eliminar la Tarea: " + tasks.get(table.getSelectedRow()).getNombre()) == 0) {
					
					if (usersBean.DeleteRoleTask(tasks.get(table.getSelectedRow()), roles.get(comboBoxRoles.getSelectedIndex() - 1))) {
						
						usersBean.DeleteTask(tasks.get(table.getSelectedRow()).getIdTarea());
						
						JOptionPane.showMessageDialog(null, "La tarea ha sido quitada del rol y eliminada exiotasemente");
						
						//Se refresca la tabla con la relacion eliminada
						LoadTasks();
						
					}	
																							
				}
				
			}
			
		}	
	
	public RolesDialog() throws NamingException {
		setResizable(false);
		getContentPane().setBackground(new Color(0, 153, 102));
		setTitle("AgroDB: Roles");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(new Color(0, 0, 153));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
			{
				JSplitPane splitTasksButtons = new JSplitPane();
				splitTasksButtons.setBackground(new Color(0, 153, 102));
				splitTasksButtons.setOrientation(JSplitPane.VERTICAL_SPLIT);
				buttonPane.add(splitTasksButtons);
				{
					JSplitPane splitTaskButtonsActions = new JSplitPane();
					splitTaskButtonsActions.setBackground(new Color(0, 153, 102));
					splitTasksButtons.setLeftComponent(splitTaskButtonsActions);
					{
						JButton btnTaskModificar = new JButton("Modificar Tarea");
						btnTaskModificar.setFont(new Font("Noto Sans", Font.PLAIN, 12));
						btnTaskModificar.setBackground(new Color(255, 255, 255));
						btnTaskModificar.setForeground(new Color(102, 102, 102));
						btnTaskModificar.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								try { UpdateTask(); } catch (ServiciosException | ViewsException e) { JOptionPane.showMessageDialog(null, e.getMessage()); }
							}
						});
						splitTaskButtonsActions.setLeftComponent(btnTaskModificar);
					}
					{
						JButton btnTaskEliminar = new JButton("Eliminar Tarea");
						btnTaskEliminar.setFont(new Font("Noto Sans", Font.PLAIN, 12));
						btnTaskEliminar.setBackground(new Color(255, 255, 255));
						btnTaskEliminar.setForeground(new Color(102, 102, 102));
						btnTaskEliminar.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								try { DeleteTask(); } catch (ServiciosException | ViewsException e) { JOptionPane.showMessageDialog(null, e.getMessage()); }
							}
						});
						splitTaskButtonsActions.setRightComponent(btnTaskEliminar);
					}
				}
				{
					JButton btnTaskCrear = new JButton("Asignar Tarea");
					btnTaskCrear.setFont(new Font("Noto Sans", Font.PLAIN, 12));
					btnTaskCrear.setBackground(new Color(255, 255, 255));
					btnTaskCrear.setForeground(new Color(102, 102, 102));
					btnTaskCrear.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							try { CreateTask(); } catch (ServiciosException | ViewsException e) { JOptionPane.showMessageDialog(null, e.getMessage()); }
						}
					});
					splitTasksButtons.setRightComponent(btnTaskCrear);
				}
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(new Color(0, 0, 153));
			getContentPane().add(buttonPane, BorderLayout.NORTH);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			{
				comboBoxRoles = new JComboBox<String>();
				comboBoxRoles.setFont(new Font("Noto Mono", Font.ITALIC, 14));
				comboBoxRoles.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						LoadTasks();
					}
				});
				buttonPane.add(comboBoxRoles);
			}
			{
				JSplitPane splitRoleButtons = new JSplitPane();
				splitRoleButtons.setBackground(new Color(0, 153, 102));
				splitRoleButtons.setOrientation(JSplitPane.VERTICAL_SPLIT);
				buttonPane.add(splitRoleButtons);
				{
					JSplitPane splitRoleButtonsActions = new JSplitPane();
					splitRoleButtonsActions.setBackground(new Color(0, 153, 102));
					splitRoleButtons.setLeftComponent(splitRoleButtonsActions);
					{
						JButton btnRoleModificar = new JButton("Modificar Rol");
						btnRoleModificar.setFont(new Font("Noto Sans", Font.PLAIN, 12));
						btnRoleModificar.setBackground(new Color(255, 255, 255));
						btnRoleModificar.setForeground(new Color(102, 102, 102));
						btnRoleModificar.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								try { UpdateRole(); } catch (ServiciosException | ViewsException e) { JOptionPane.showMessageDialog(null, e.getMessage()); }
							}
						});
						splitRoleButtonsActions.setLeftComponent(btnRoleModificar);
					}
					{
						JButton btnRoleEliminar = new JButton("Eliminar Rol");
						btnRoleEliminar.setFont(new Font("Noto Sans", Font.PLAIN, 12));
						btnRoleEliminar.setBackground(new Color(255, 255, 255));
						btnRoleEliminar.setForeground(new Color(102, 102, 102));
						btnRoleEliminar.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								try { DeleteRole(); } catch (ServiciosException | ViewsException e) { JOptionPane.showMessageDialog(null, e.getMessage()); }
							}
						});
						splitRoleButtonsActions.setRightComponent(btnRoleEliminar);
					}
				}
				{
					JButton btnRoleCrear = new JButton("Crear Rol");
					btnRoleCrear.setFont(new Font("Noto Sans", Font.PLAIN, 12));
					btnRoleCrear.setBackground(new Color(255, 255, 255));
					btnRoleCrear.setForeground(new Color(102, 102, 102));
					btnRoleCrear.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							try { CreateRole(); } catch (ServiciosException | ViewsException e) { JOptionPane.showMessageDialog(null, e.getMessage()); }
						}
					});
					splitRoleButtons.setRightComponent(btnRoleCrear);
				}
			}
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			getContentPane().add(scrollPane, BorderLayout.CENTER);
			{
				table = new JTable();
				scrollPane.setViewportView(table);
				table.setForeground(new Color(102, 102, 102));
				table.setFont(new Font("Noto Sans", Font.PLAIN, 14));
				table.setBackground(new Color(255, 255, 255));
				table.setModel(new DefaultTableModel(
					new Object[][] {
					},
					new String[] {
						"Tareas"
					}
				));
			}
		}
		
		LoadRoles();
		
	}

}
