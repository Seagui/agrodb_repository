package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import exceptions.ServiciosException;

import javax.swing.JTextArea;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JScrollPane;

import model.Casilla;
import model.Formulario;
import services.CasillasBeanRemote;
import services.FormulariosBeanRemote;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class CasillasList extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private final JPanel contentPanel = new JPanel();
	JPanel panelCasillas;
	
	FormulariosBeanRemote formsBean = (FormulariosBeanRemote) InitialContext.doLookup("AgroDB_Server/FormulariosBean!services.FormulariosBeanRemote");
	CasillasBeanRemote boxsBean = (CasillasBeanRemote) InitialContext.doLookup("AgroDB_Server/CasillasBean!services.CasillasBeanRemote");
	
	List<Casilla> boxs;
	List<Casilla> allBoxs;
	List<JCheckBox> checkBoxes = new ArrayList<JCheckBox>();
	List<Boolean> oldBoxs = new ArrayList<Boolean>();
	
	private void LlenarCasillas(Formulario formulario) {

		//Se seleccionan las casillas
		boxs = boxsBean.SelectBoxs(formulario.getNombFormulario());
		allBoxs = boxsBean.SelectBoxs();
		
		//Se crean los checkboxes
		for (Casilla box : allBoxs) {
			JCheckBox newCheckBox = new JCheckBox();
			newCheckBox.setText(box.getParametro());
			checkBoxes.add(newCheckBox);
			panelCasillas.add(newCheckBox);
		}
		
		//Se marcan los seleccionados
		int index = 0;
		for (Casilla box : allBoxs) {
			for (Casilla boxChecked : boxs) {
				if (boxChecked.getParametro().equals(box.getParametro())) {
					checkBoxes.get(index).setSelected(true);
				}
			}
			index++;
		}
		
		//Se guarda cuales estan seleccionados
		for (JCheckBox ch : checkBoxes) {
			oldBoxs.add(ch.isSelected());
		}
				
		JCheckBox usuario = new JCheckBox();
		usuario.setText("Usuario");
		usuario.setSelected(true);
		usuario.setEnabled(false);
		panelCasillas.add(usuario);
		
		JCheckBox fecha = new JCheckBox();
		fecha.setText("Fecha");
		fecha.setSelected(true);
		fecha.setEnabled(false);
		panelCasillas.add(fecha);
		
		JCheckBox metodoMuestreo = new JCheckBox();
		metodoMuestreo.setText("Metodo  de Muestreo");
		metodoMuestreo.setSelected(true);
		metodoMuestreo.setEnabled(false);
		panelCasillas.add(metodoMuestreo);

		JCheckBox estacionMuestreo = new JCheckBox();
		estacionMuestreo.setText("Estacion de Muestreo");
		estacionMuestreo.setSelected(true);
		estacionMuestreo.setEnabled(false);
		panelCasillas.add(estacionMuestreo);

		JCheckBox departamento = new JCheckBox();
		departamento.setText("Departamento");
		departamento.setSelected(true);
		departamento.setEnabled(false);
		panelCasillas.add(departamento);
		
	}
	
	public void MakeChanges(Formulario formulario) throws ServiciosException, NamingException {
		
		//Se verifican los cambios
		int index = 0;
		for (JCheckBox ch : checkBoxes) {
			
			if (ch.isSelected() != oldBoxs.get(index)) {
								
				if (ch.isSelected() && (!oldBoxs.get(index))) {	//Si fue selecionada
					
					//Se a�ade la casilla
					formsBean.AddBox(allBoxs.get(index), formulario);
										
				}
				else if ((!ch.isSelected()) && oldBoxs.get(index)) {	//Si fue deselecionada
										
					//Se elimina la casilla
					formsBean.RemoveBox(allBoxs.get(index), formulario);
										
				}
				
			}
			
			index++;
		}
		
		//Se actualiza la tabla
		ViewForm.LoadBoxs();
		dispose();
		
	}

	public CasillasList(Formulario formulario) throws NamingException {
		setResizable(false);
		setTitle("AgroDB: Lista Casillas");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(0, 0, 153));
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		{
			JTextArea txtrSeleccionaLaCasilla = new JTextArea();
			txtrSeleccionaLaCasilla.setText("Selecciona la casilla que quieres agregar\r\n a tu formulario y luego confirma.");
			txtrSeleccionaLaCasilla.setFont(new Font("Comic Sans MS", Font.BOLD, 17));
			contentPanel.add(txtrSeleccionaLaCasilla);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(new Color(0, 0, 153));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Confirmar");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try {
							MakeChanges(formulario);
						} catch (ServiciosException | NamingException e) {
							JOptionPane.showMessageDialog(null, e.getMessage());
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			getContentPane().add(scrollPane, BorderLayout.CENTER);
			{
				panelCasillas = new JPanel();
				scrollPane.setViewportView(panelCasillas);
			}
		}
		
		LlenarCasillas(formulario);
	}

}
