package views;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.Box;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.HeadlessException;

import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import exceptions.ServiciosException;

import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.Formulario;
import model.Tarea;
import services.FormulariosBeanRemote;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JScrollPane;

public class FormulariosList {

	private JFrame frame;
	private static JTable tablaListaForm;
	JButton EditarForm;
	List<Tarea> task;

	FormulariosBeanRemote formsBean = (FormulariosBeanRemote) InitialContext.doLookup("AgroDB_Server/FormulariosBean!services.FormulariosBeanRemote");
	
	static List<Formulario> forms;
	
	public FormulariosList(List<Tarea> tasks) throws NamingException {
		initialize();
		
		HideButtons(tasks);
		
  	  	LoadForms();
		
		frame.setVisible(true);
	}

	private void HideButtons(List<Tarea> tasks) {
		
		task = tasks;
		
		//Se muestra el boton de editar si se tiene acceso
		for (Tarea task : tasks) {
			if (task.getNombre().equals("Gestion de Formularios")) {
				EditarForm.setVisible(true);
			}	
		}
		
	}
	
	static private void LoadForms() throws NamingException {
		
		//Se obtienen los formularios
		FormulariosBeanRemote formsBean = (FormulariosBeanRemote) InitialContext.doLookup("AgroDB_Server/FormulariosBean!services.FormulariosBeanRemote");
		forms = formsBean.SelectForms();
		
		DefaultTableModel model = (DefaultTableModel) tablaListaForm.getModel();
		
		//Se vacia la tabla
        int rows = tablaListaForm.getRowCount();
        for (int i = rows-1; i >= 0; i--) {
            model.removeRow(i);
        }
		
        //Se carga la tabla
		for (Formulario form : forms) {
			model.addRow(new Object[]{
					form.getNombFormulario(),
					form.getResumen()
			});
		}
		
	}
	
	private void UpdateForm() throws HeadlessException, ServiciosException, NamingException {
		
		  //Se obtienen los datos
	      JTextField textFieldNombre = new JTextField(20);
	      textFieldNombre.setText(forms.get(tablaListaForm.getSelectedRow()).getNombFormulario());
	      
	      JTextArea textAreaResumen = new JTextArea(2, 40);
	      textAreaResumen.setText(forms.get(tablaListaForm.getSelectedRow()).getResumen());

	      JPanel panelContenedor = new JPanel();
	      
	      panelContenedor.add(new JLabel("Nombre:"));
	      panelContenedor.add(textFieldNombre);
	      
	      panelContenedor.add(Box.createHorizontalStrut(15));
	      
	      panelContenedor.add(new JLabel("Resumen:"));
	      panelContenedor.add(textAreaResumen);

	      int result = JOptionPane.showConfirmDialog(null, panelContenedor, 
	               "Modificacion del Formulario: " + forms.get(tablaListaForm.getSelectedRow()).getNombFormulario(), JOptionPane.OK_CANCEL_OPTION);
	      
	      if (result == JOptionPane.OK_OPTION) {
	    	  
	    	  //Se verifica se haya realizado algun cambio
	    	  if (textFieldNombre.getText().equals(forms.get(tablaListaForm.getSelectedRow()).getNombFormulario()) && textAreaResumen.getText().equals(forms.get(tablaListaForm.getSelectedRow()).getResumen())) {
	    		
	    		  JOptionPane.showMessageDialog(null, "No se ha realizado ningun cambio");
	    	  
	    	  }
	    	  else {
	    		
		    	  //Se verifica el nombre no este vacio
		    	  if (textFieldNombre.getText().isEmpty()) {
		    		  JOptionPane.showMessageDialog(null, "No puede dejar vacio el campo 'Nombre'");
		    	  } 
		    	  else {
			    	  
		    		  //Se crea el formulario
			    	  if (formsBean.UpdateForm(textFieldNombre.getText(), textAreaResumen.getText(), forms.get(tablaListaForm.getSelectedRow()).getUsuario(),
			    			  forms.get(tablaListaForm.getSelectedRow()).getIdFormulario(), forms.get(tablaListaForm.getSelectedRow()).getNombFormulario())) {
			    		  
			    		  JOptionPane.showMessageDialog(null, "El formulario se modifico exitosamente");
			    		  
				    	  LoadForms();
				    	  
			    	  }
		    		  
		    	  }
	    		  
	    	  }	    	  

	    		    	  
	      }
		
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JTextArea txtrListadoDeFormularios = new JTextArea();
		frame.getContentPane().add(txtrListadoDeFormularios, BorderLayout.NORTH);
		txtrListadoDeFormularios.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		txtrListadoDeFormularios.setText("Listado de formularios (Plantillas)");
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 153));
		frame.getContentPane().add(panel, BorderLayout.SOUTH);
		
		JButton VerFormulario = new JButton("Ver");
		VerFormulario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (tablaListaForm.getSelectedRow() >= 0) {
					try {
						@SuppressWarnings("unused")
						ViewForm vf = new ViewForm(forms.get(tablaListaForm.getSelectedRow()), task);
						frame.dispose();
					} catch (NamingException e) {
						e.printStackTrace();
					}
				}
			}
		});
		VerFormulario.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel.add(VerFormulario);
		
		EditarForm = new JButton("Modificar");
		panel.add(EditarForm);
		EditarForm.setFont(new Font("Tahoma", Font.PLAIN, 15));
		EditarForm.setVisible(false);
		EditarForm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tablaListaForm.getSelectedRow() >= 0) {
					try {
						UpdateForm();
					} catch (HeadlessException | ServiciosException | NamingException e1) {
						JOptionPane.showMessageDialog(null, e1.getMessage());
					}
				}
			}
		});
		
		JButton VolverFormulario = new JButton("Volver");
		panel.add(VolverFormulario);
		VolverFormulario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Home.frame.setVisible(true);
				frame.dispose();
			}
		});
		VolverFormulario.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JScrollPane scrollPane = new JScrollPane();
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		tablaListaForm = new JTable();
		scrollPane.setViewportView(tablaListaForm);
		tablaListaForm.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nombre", "Resumen"
			}
		));
	}
}
