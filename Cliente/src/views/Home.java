package views;

import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import exceptions.ServiciosException;
import exceptions.ViewsException;
import model.Tarea;
import model.Usuario;
import services.FormulariosBeanRemote;
import services.UsuariosBeanRemote;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;

public class Home {

	static JFrame frame;
	
	private static JMenuBar menuBar;
	private static JMenuItem mntmCrearU;
	private static JMenuItem mntmRole;
	private static JMenuItem mntmListarU;
	private static JMenuItem mntmCrearF;
	private static JMenuItem mntmListarF;
	private static JMenuItem mntmCrearC;
	private static JMenu mnReportes;
	private static JLabel lblUsuario;

	private JMenuItem mntmReportes;
	private JLabel lbImagen;
	
	UsuariosBeanRemote usersBean = (UsuariosBeanRemote) InitialContext.doLookup("AgroDB_Server/UsuariosBean!services.UsuariosBeanRemote");
	FormulariosBeanRemote formsBean = (FormulariosBeanRemote) InitialContext.doLookup("AgroDB_Server/FormulariosBean!services.FormulariosBeanRemote");
	
	static Usuario user;
	static List<Tarea> tasks;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					@SuppressWarnings("unused")
					Home window = new Home();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public Home() throws NamingException {				
		initialize();
		
		CreateLogin();	// Se solicita Logeo al usuario
	}

	// Metodo que instancia la ventana de Login
	private void CreateLogin() throws NamingException {
		
		Login l = new Login(); 
		l.setVisible(true); 
	
	}
	
	// Metodo que se encarga de mostrar las zonas a las que el Usuario tiene acceso
	public static void RoleConfiguration(Usuario userLogged) throws NamingException {

		// Se ocultan los botones
		HideButtons();
		
		user = userLogged;	// Se guarda el Usuario Logeado
		
		lblUsuario.setText("Usuario: " + user.getNombUsuario());	// Se modifica el texto para que muestre el Usuario Logeado
		
		menuBar.setVisible(true);	// Se visibilisa el menu
		
		// Se utiliza el servicio de Usuarios para obtener las tareas del usuario
		UsuariosBeanRemote usersBean = (UsuariosBeanRemote) InitialContext.doLookup("AgroDB_Server/UsuariosBean!services.UsuariosBeanRemote");
		tasks = usersBean.SelectTasks(user.getRole().getNombre());
				
		// Se muestran las secciones a las que el Usuario puede acceder dependiendo de las tareas que tiene
		for (Tarea task : tasks) {
			if (task.getNombre().equals("Gestion de Usuarios")) {
				mntmCrearU.setVisible(true);
				mntmListarU.setVisible(true);
			}
			if (task.getNombre().equals("Gestion de Roles") || task.getNombre().equals("Gestion de Tareas"))
				mntmRole.setVisible(true);
			if (task.getNombre().equals("Gestion de Reportes"))
				mnReportes.setVisible(true);
			if (task.getNombre().equals("Gestion de Formularios") || task.getNombre().equals("Gestion de Actividades"))
				mntmListarF.setVisible(true);
			if (task.getNombre().equals("Gestion de Casillas"))
				mntmCrearC.setVisible(true);
			if (task.getNombre().equals("Gestion de Formularios"))
				mntmCrearF.setVisible(true);
		}
		
		// Se muestra la ventana
		frame.setVisible(true);
				
	}
	
	private static void HideButtons() {
		mntmCrearU.setVisible(false);
		mntmListarU.setVisible(false);
		mntmRole.setVisible(false);
		mnReportes.setVisible(false);
		mntmListarF.setVisible(false);
		mntmCrearC.setVisible(false);
		mntmCrearF.setVisible(false);
	}
	
	// Metodo para modificar la contraseņa del Usuario logeado
	private void UpdatePassword() throws ViewsException, ServiciosException {
		
		// Se muestra una ventana solicitando la nueva Contraseņa
		JPasswordField pf = new JPasswordField();
		int okCxl = JOptionPane.showConfirmDialog(null, pf, "Escriba su nueva Contraseņa", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

		// Si apreta en modificar
		if (okCxl == JOptionPane.OK_OPTION) {
		  String password = new String(pf.getPassword());
		  
		  // Si el contraseņa no es nulo, se interpreta quiere ingresar algo
		  if (password != null) {
			  
			  // Se verifica que la contraseņa
			  if (VerifyPassword(password)) {
				  
				  // Se utiliza los servicios de Usuario para modificar el Usuario con la nueva contraseņa
				  if (usersBean.UpdateUserPassword(user, password)) {
					  
					  JOptionPane.showMessageDialog(null, "La contraseņa ha sido modificada correctamente");
					  
				  }
				  
			  }														
				
		  }
		}
						
	}
	
	// Metodo para verificar las contraseņas
	public static boolean VerifyPassword(String password) throws ViewsException {
		
		// Se verifica que la contraseņa no este vacia
		if (password.trim().equals(""))
			throw new ViewsException("La contraseņa no puede estar vacia");
		
		// Verificar si tiene almenos 8 caracteres
		if (password.length() < 8)
			throw new ViewsException("La contraseņa debe de tener ocho o mas caracteres");
		
		// Verificar si tiene caracteres
		if (!(password.matches(".*[a-z].*")))
			throw new ViewsException("La contraseņa debe de tener almenos una letra");
		
		// Verificar si tiene numeros
		if (!(password.matches(".*[0-9].*")))
			throw new ViewsException("La contraseņa debe de tener alemnos un numero");
		
		return true;
	}
	
	// Metodo para crear un Formulario
	private void CreateForm() throws ViewsException {
		  // Se crean los campos
	      JTextField textFieldNombre = new JTextField(20);
	      JTextArea textAreaResumen = new JTextArea(2, 40);
	      
	      // Se crea el panel
	      JPanel panelContenedor = new JPanel();
	      
	      // Se aņaden los campos al panel junto con labels
	      panelContenedor.add(new JLabel("Nombre:"));
	      panelContenedor.add(textFieldNombre);
	      
	      panelContenedor.add(Box.createHorizontalStrut(15));
	      
	      panelContenedor.add(new JLabel("Resumen:"));
	      panelContenedor.add(textAreaResumen);

	      // Se muestra el mensaje con el panel creado
	      int result = JOptionPane.showConfirmDialog(null, panelContenedor, 
	               "Creacion de un Formulario", JOptionPane.OK_CANCEL_OPTION);
	      
	      if (result == JOptionPane.OK_OPTION) {
	    	  
	    	  // Se verifica el nombre no este vacio
	    	  if (textFieldNombre.getText().isEmpty())
	  			throw new ViewsException("No puede dejar vacio el campo 'Nombre'");	    	
	    	  
		      try {
					
		    	  // Se crea el formulario utilizando el servicio de formularios
		    	  if (formsBean.CreateForm(textFieldNombre.getText(), textAreaResumen.getText(), user)) {
						  
					  JOptionPane.showMessageDialog(null, "El formulario ha sido creado correctamente");
						  
				  }
		    		  
			} catch (HeadlessException | ServiciosException e) { JOptionPane.showMessageDialog(null, e.getMessage()); }
		    	  	    	  			         
	      }
	}

	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.getContentPane().setBackground(new Color(0, 102, 204));
		
		lbImagen = new JLabel("");
		lbImagen.setIcon(new ImageIcon("D:\\Java\\Cliente\\AgroDB_Client\\Img\\UTECInicio.jpg"));
		frame.getContentPane().add(lbImagen, BorderLayout.CENTER);
		frame.setTitle("AgroDB: Inicio");
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		menuBar = new JMenuBar();
		menuBar.setFont(new Font("Noto Mono", Font.PLAIN, 14));
		menuBar.setBackground(new Color(0, 102, 204));
		frame.setJMenuBar(menuBar);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.setFont(new Font("Noto Sans", Font.PLAIN, 12));
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	// Evento del boton de Logout
				try {
					menuBar.setVisible(false); // Se oculta el menu
					CreateLogin(); // Se llama al Login
				} catch (NamingException e) { e.printStackTrace(); }
			}
		});
		menuBar.add(btnLogout);
				
		JMenu mnUsuarios = new JMenu("Usuarios");
		mnUsuarios.setForeground(new Color(255, 255, 255));
		mnUsuarios.setFont(new Font("Noto Mono", Font.PLAIN, 12));
		mnUsuarios.setBackground(new Color(0, 153, 0));
		menuBar.add(mnUsuarios);
		
		mntmCrearU = new JMenuItem("Crear Usuario");
		mntmCrearU.setFont(new Font("Noto Mono", Font.PLAIN, 11));
		mntmCrearU.addActionListener(new ActionListener() {	// Evento del boton de Crear Usuario
			//Se va al frame de alta de usuarios
			public void actionPerformed(ActionEvent arg0) {
				try {
					UsersDialog u = new UsersDialog(); 
					u.setModal(true);
					u.setVisible(true); 
				} catch (NamingException e) { e.printStackTrace(); }
			}
		});
		mntmCrearU.setVisible(false);
		
		mntmRole = new JMenuItem("Gestionar Roles");
		mntmRole.setFont(new Font("Noto Mono", Font.PLAIN, 11));
		mntmRole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	// Evento del boton de Gestionar Roles
				try {
					RolesDialog r = new RolesDialog();
					r.setModal(true);
					r.setVisible(true); 
				} catch (NamingException e) { e.printStackTrace(); } 
			}
		});
		mntmRole.setVisible(false);
		mnUsuarios.add(mntmRole);
		mnUsuarios.add(mntmCrearU);
		
		mntmListarU = new JMenuItem("Listar Usuarios");
		mntmListarU.setFont(new Font("Noto Mono", Font.PLAIN, 11));
		mntmListarU.addActionListener(new ActionListener() {	// Evento del boton de Listar Formularios
			//Se va a el frame de listado de usuarios
			public void actionPerformed(ActionEvent arg0) {
				try {
					@SuppressWarnings("unused")
					UsersList ul = new UsersList();
					frame.setVisible(false);
				} catch (NamingException e) { e.printStackTrace(); }
			}
		});
		mntmListarU.setVisible(false);
		mnUsuarios.add(mntmListarU);
		
		JMenuItem mntmContrasenia = new JMenuItem("Modificar Contrase\u00F1a");
		mntmContrasenia.setFont(new Font("Noto Mono", Font.PLAIN, 11));
		mntmContrasenia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	// Evento del boton de Modificar Contraseņa
				try { UpdatePassword(); } catch (ViewsException | ServiciosException e) { JOptionPane.showMessageDialog(null, e.getMessage()); }
			}
		});
		mnUsuarios.add(mntmContrasenia);
		
		JMenu mnFormularios = new JMenu("Formularios");
		mnFormularios.setForeground(new Color(255, 255, 255));
		mnFormularios.setFont(new Font("Noto Mono", Font.PLAIN, 12));
		menuBar.add(mnFormularios);
		
		mntmCrearF = new JMenuItem("Crear Formulario");
		mntmCrearF.addActionListener(new ActionListener() {	// Evento del boton de Crear Formulario
			public void actionPerformed(ActionEvent arg0) {
				try {
					CreateForm();
				} catch (ViewsException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		});
		mntmCrearF.setFont(new Font("Noto Mono", Font.PLAIN, 11));
		mntmCrearF.setVisible(false);
		mnFormularios.add(mntmCrearF);
		
		mntmListarF = new JMenuItem("Listar Formularios");
		mntmListarF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					@SuppressWarnings("unused")
					FormulariosList f = new FormulariosList(tasks);
					frame.setVisible(false);
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
		});
		mntmListarF.setFont(new Font("Noto Mono", Font.PLAIN, 11));
		mntmListarF.setVisible(false);
		mnFormularios.add(mntmListarF);
		
		mntmCrearC = new JMenuItem("Crear Casilla");
		mntmCrearC.setFont(new Font("Noto Mono", Font.PLAIN, 11));
		mntmCrearC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					CasillasDialog cc = new CasillasDialog();
					cc.setModal(true);
					cc.setVisible(true); 				
				} catch (NamingException e) {
					e.printStackTrace();
				} 
			}
		});
		mnFormularios.add(mntmCrearC);
		mntmCrearC.setVisible(false);
		
		mnReportes = new JMenu("Reportes");
		mnReportes.setForeground(new Color(255, 255, 255));
		mnReportes.setFont(new Font("Noto Mono", Font.PLAIN, 12));
		mnReportes.setVisible(false);
		menuBar.add(mnReportes);
		
		mntmReportes = new JMenuItem("Analisis de Muestreos");
		mntmReportes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					@SuppressWarnings("unused")
					Reports r = new Reports();
					frame.setVisible(false);
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
		});
		mnReportes.add(mntmReportes);
		
		lblUsuario = new JLabel("Usuario: Usuario");
		lblUsuario.setForeground(new Color(255, 255, 255));
		lblUsuario.setFont(new Font("Noto Sans", Font.ITALIC, 12));
		menuBar.add(lblUsuario);
		
		//Se setea el menuBar como invisible hasta que se confirme un login exitoso
		menuBar.setVisible(false);
		
	}

}