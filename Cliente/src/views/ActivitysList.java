package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import services.ActiCampoBeanRemote;
import services.CasillasBeanRemote;
import model.ActiCampo;
import model.Casilla;
import model.Dato;
import model.Formulario;

import java.awt.Color;
import javax.swing.JScrollPane;

public class ActivitysList extends JDialog {
	
	private static final long serialVersionUID = 1L;
	
	private JTable table;
	JLabel lblFormulario;

	ActiCampoBeanRemote activitysBean = (ActiCampoBeanRemote) InitialContext.doLookup("AgroDB_Server/ActiCampoBean!services.ActiCampoBeanRemote");
	CasillasBeanRemote boxsBean = (CasillasBeanRemote) InitialContext.doLookup("AgroDB_Server/CasillasBean!services.CasillasBeanRemote");
	
	List<ActiCampo> activitys;
	private JScrollPane scrollPane;
	
	private void LoadActivitys(Formulario form) {

		//Se configura el titulo
		lblFormulario.setText("Formulario: " + form.getNombFormulario());
		
		//Se obtienen las actividades
		activitys = activitysBean.SelectActivitys(form.getNombFormulario(), Home.user);
						
		DefaultTableModel model = (DefaultTableModel) table.getModel();
			
		//Se vacia la tabla
        int rows = table.getRowCount();
        for (int i = rows-1; i >= 0; i--) {
        	model.removeRow(i);
        }
        
        //Se a�aden la columnas de los parametros extras
        for (Casilla box : boxsBean.SelectBoxs(form.getNombFormulario())) {
			model.addColumn(box.getParametro());
        }
        
        //Se a�aden los valores estandar
        for (ActiCampo activity : activitys) {						
        	model.addRow(new Object[]{
        			activity.getUsuario().getNombre(),
        			activity.getFecha(),
        			activity.getMetoMuestreo().getMetodo(),
        			activity.getEstaMuestreo().getEstacion(),
        			activity.getZona().getZona()
        			});
        }
        
        //Se a�aden los valores extra
        int row = 0;
        for (ActiCampo actiCampo : activitys) {

        	int column = 5;
            for (Casilla box : boxsBean.SelectBoxs(form.getNombFormulario())) {
    			
            	for (Dato dato : activitysBean.SelectData(actiCampo)) {
            		
            		if (dato.getCasilla().getParametro().equals(box.getParametro())) {
            			
                		model.setValueAt(dato.getValor(), row, column);
  
            		}
            		
            	}
            	
        		column++;
            }
        	
			row++;
		}
		
	}
	
	public ActivitysList(Formulario form) throws NamingException {
		setTitle("AgroDB: Lista Actividades");
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setForeground(new Color(255, 255, 255));
			buttonPane.setBackground(new Color(0, 0, 153));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.NORTH);
			{
				lblFormulario = new JLabel("Formulario:");
				lblFormulario.setForeground(new Color(255, 255, 255));
				lblFormulario.setHorizontalAlignment(SwingConstants.CENTER);
				buttonPane.add(lblFormulario);
			}
		}
		{
			scrollPane = new JScrollPane();
			getContentPane().add(scrollPane, BorderLayout.CENTER);
			{
				table = new JTable();
				scrollPane.setViewportView(table);
				table.setModel(new DefaultTableModel(
					new Object[][] {
					},
					new String[] {
						"Usuario", "Fecha", "Metodo Muestreo", "Estacion Muestreo", "Zona"
					}
				));
			}
		}
				
		LoadActivitys(form);
	}

}
